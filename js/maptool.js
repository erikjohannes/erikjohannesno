var map = L.map('map').setView([65.50, 13.00], 4);
L.tileLayer('https://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=topo4&zoom={z}&x={x}&y={y}', {
    attribution: '<a href="http://www.kartverket.no/">Kartverket</a>'
}).addTo(map);
var mapControl = L.control.layers(null, null).addTo(map);

function plotGPX(map, control, url) {
    var yo = new L.GPX(url, {
        async: true,
        marker_options: {
            startIconUrl: 'img/pin-icon-start.png',
            endIconUrl:   'img/pin-icon-end.png',
            shadowUrl:    'img/pin-shadow.png',
            //iconSize: [16, 25],
            //shadowSize: [25, 25],
            //iconAnchor: [32, 90],
            //shadowAnchor: [32, 94],
            clickable: true,
            showRouteInfo: true
        },
    }).addTo(map);
}

// document.getElementById('file-selector').addEventListener('change', function(e) {
document.getElementById('import').onclick = function () {

    var files = document.getElementById('file-selector').files;
    var importedFileName = files[0].name;
    console.log(importedFileName);
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(importedFileName)[1];

    var reader = new FileReader();

    reader.onload = function (e) {

        // Reset arrays in case user clicks "Import" multiple times
        peaks_visited = [];
        peaks_unvisited = [];

        if (ext === "gpx") {
            // Parse gpx file.
            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(e.target.result, "text/xml");

            plotGPX(map, mapControl, xmlDoc);
        } else {
            window.alert("File most be .gpx or .json.");
        }
    }
    reader.readAsText(files.item(0));

};
// });
