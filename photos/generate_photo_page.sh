#!/bin/bash
# ===================================================================
# File:     generate_photo_page.sh
# Author:   Erik Johannes Husom
# Created:
# -------------------------------------------------------------------
# Description: Generate a photo page from a directories of photos.
# ===================================================================

display_help() {
    
    echo "script title"
    echo
    echo "Usage: script [options]"
    echo "   or: script [arguments]"
    echo
    echo "Arguments:"
    echo "  -h, --help      Print Help (this message)"
    echo
    echo "Description:"
    echo "  Description..."
    echo

    exit 0

}

generate_photo_page () {
    
        # Get the current directory
        dir=$(pwd)
    
        # Get the name of the directory
        dir_name=$(basename $dir)
    
        # Create the HTML file
        echo "<!DOCTYPE html>" > index.html
        echo "<html>" >> index.html
        echo "<head>" >> index.html
        echo "<title>Photos – Erik Johannes Husom</title>" >> index.html
        echo "<style>" >> index.html
        echo "body { background-color: #000; text-align: center; }" >> index.html
        # echo "div { display: flex; flex-wrap: wrap; }" >> index.html
        echo "img { width: 300px; height: 300px; object-fit: cover; }" >> index.html
        # Add alternative image size for mobile devices
        echo "@media only screen and (max-width: 600px) { img { width: 100%; height: auto; } }" >> index.html

        echo "</style>" >> index.html
        echo "</head>" >> index.html
        echo "<body>" >> index.html
        echo "<div>" >> index.html
    
        # Loop through all files in the directory in reverse, to get the newest files first
        for file in $(ls -r); do
    
            # If the file is a directory, skip it
            if [ -d "$file" ]; then
                continue
            fi
    
            # Get the file extension
            ext="${file##*.}"
    
            # If the file is a JPG, PNG, or GIF, add it to the HTML file
            if [ "$ext" == "jpg" ] || [ "$ext" == "png" ] || [ "$ext" == "gif" ] || [ "$ext" == "jpeg" ] || [ "$ext" == "JPG" ]; then
                echo "<a href=\"$file\"><img src=\"$file\" alt=\"$file\"></a>" >> index.html
            fi
    
        done
    
        # Close the HTML file
        echo "</div>" >> index.html
        echo "</body>" >> index.html
        echo "</html>" >> index.html

}

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    display_help
else
    generate_photo_page
fi


