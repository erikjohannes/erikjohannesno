---
title: "KI og klimaavtrykk på NRKs Abels Tårn"
date: 2025-02-10T07:47:12+01:00
type: ["posts"]
draft: false
tags:
language: norwegian
categories:
---

Forrige uke var jeg så heldig å få fortelle om vår forskning innen mer bærekraftig kunstig intelligens på en av Norges største populærvitenskapelige radioprogram, nemlig Abels Tårn på NRK!

Først og fremst var det veldig gøy å få være med, og det var en flott sjanse til å fortelle om hva jeg holder på med til daglig. Ikke mange er klar over at det jobbes aktivt i Norge med forskning for å adressere klimaavtrykket til KI, og på min forskningsgruppe (Green IoT) leder vi forskningen innen «grønn KI» i EU-prosjektet ENFIELD[^1]. En av de tingene vi driver med akkurat nå er å kartlegge energiforbruket til ulike generative språkmodeller, og hvordan ulike faktorer påvirker både energieffektiviteten og ytelsen.

Er det en ting som er sikkert, så er det at det er mange estimater, usikre tall, og manglende kildehenvisninger ute og går når det gjelder strømforbruk og klimaavtrykk KI-modeller, spesielt språkmodellene bak ChatGPT. Generelt sett har vi så lite kunnskapsgrunnlag om modellene til OpenAI, at vi kan ikke si med sikkerhet hvor mye en ChatGPT-spørring (-prompt) koster i verken strøm, vann eller karbonutslipp.
Likevel må man innimellom prøve å konkretisere forbruket litt ved å ta i bruk slike estimater, selv om det ikke alltid er så lett å forklare konsist hvordan disse beregningene er gjort. Utviklingen går så fort vil de fleste estimater foreldes ganske raskt, med nye språkmodeller som dukker opp.

Det overordnede bildet blir likevel stående: Generativ KI har et signifikant ressursforbruk som vi er nødt til å ta hensyn til når vi vurderer hva vi skal bruke denne teknologien til. Regulering og åpenhet trengs for å adressere problemet.

Jeg håper intervjuet kan være med på å gjøre KI litt mindre mystisk, litt mer håndfast, og å forklare hvorfor klimaavtrykket til generativ KI er så stort sammenlignet med andre former for digital teknologi.

Tusen takk til radiovertene i Abels Tårn, Torkild Jemterud, Kristina Aas og Hanne Aass, for å invitasjonen og en meget hyggelig samtale rundt temaet!

Hør på episoden her: [Abels Tårn 7. feb 2025 (start fra 29 min)](https://radio.nrk.no/serie/abels-taarn-radio/MDFP05001225?utm_source=nrkradio&utm_medium=delelenke-ios&utm_content=prf:MDFP05001225#t=29m4s)

[^1]: [ENFIELD-prosjektets hjemmeside](https://www.enfield-project.eu).

<!-- Lenke til opprinnelig bloggpost: --> 

<!-- #GrønnKI #KunstigIntelligens #GreenAI #ArtificialIntelligence -->
