---
title: "Links #6 AI"
date: 2023-11-28T13:27:23+02:00
type: "posts"
draft: true
---

Another collection of interesting reads from around the web, this time exclusively AI-related topics:

- [ChatGPT on your iPhone? The four reasons why this is happening far too early](https://www.theguardian.com/commentisfree/article/2024/jun/13/chatgpt-iphone-apple-technology-ready)
> The AI’s errors can still be comical and catastrophic. Do we really want this technology to be in so many pockets?
- [I am tired of AI](https://www.ontestautomation.com/i-am-tired-of-ai/)
> Over the last few years, I’ve had the honour of being the member of the program committee for three different conferences, and a one-off reviewer for one or two more. What I have seen in these years is a significant rise in proposals that were clearly written with the help of, or in many cases, entirely by ChatGPT or similar software. [...] t has gotten so bad that I, for one, immediately reject a proposal when it is clear that it was written by or with the help of AI, no matter how interesting the topic is or how good of a talk you will be able to deliver in person. I’m not taking the chance if you don’t put in the effort of writing a good proposal yourself, and I am pretty confident I’m not the only conference program committee member thinking that way.
- [ChatGPT is a blurry JPEG of the Web](https://www.newyorker.com/tech/annals-of-technology/chatgpt-is-a-blurry-jpeg-of-the-web)
> OpenAI’s chatbot offers paraphrases, whereas Google offers quotes. Which do we prefer?
- [“Web3” and “AI”](https://adactio.com/articles/20290)
> I like to think of “AI” as a kind of advanced autocomplete. I don’t say that to denigrate it. Quite the opposite. Autocomplete is something that appears mundane on the surface but has an incredible amount of complexity underneath: real-time parsing of input, a massive database of existing language, and on-the-fly predictions of the next most suitable word. Large language models do the same thing, but on a bigger scale.
