---
date: 2023-01-09T09:00:00
title: Playing in powder with Gjert
type: ["posts"]
draft: false
tags:
categories:
---

A fun afternoon skiing with my Border collie Gjert in our backyard.

{{< rawhtml >}}
<video id="video" width="1280" height="360" controls>
    <source src="posts/20230109-playing-in-powder-with-gjert/skiing-720.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video> 
{{< /rawhtml >}}
