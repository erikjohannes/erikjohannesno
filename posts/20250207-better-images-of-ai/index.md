---
title: "Better images of AI"
date: 2025-02-07T21:27:23+02:00
type: "posts"
draft: false
---

As AI is discussed in all kinds of media, futuristic images abound in news papers, presentation slides, blog posts, and social media.
The problem with many of these images is that they muddle the impression of what we are actually talking about when we discuss AI, which in many cases are simply a huge number of additions and multiplications being performed on a computer (yes, that's what the chatbot you're using as an orcale really is).

I just discovered a handy resource when you need illustrative images for AI-related topic: [Better images if AI](https://betterimagesofai.org/).
As the front page of the website says (emphasis mine):

> Abstract, futuristic or science-fiction-inspired images of AI hinder the understanding of the technology’s already significant societal and environmental impacts. Images relating machine intelligence to human intelligence set **unrealistic expectations and misstate the capabilities of AI**.

The website is run as a collaboration between multiple organizations (BBC, We and AI, Leverhulme CFI), where they share more meaningful AI-related images.
They are free to use as long as an attribution is included.

Now you don't even have to waste electricity on asking Midjourney to generate illustrations for your slides!
