---
title: "Research notes"
date: 2021-05-24T21:47:12+02:00
type: ["posts"]
draft: true
tags:
categories:
---


I work as a researcher at one of the largest research organizations in Norway, and I'm working mainly on machine learning and artificial intelligence.
We strive to be as open as possible about our research, and in many projects it's a requirement to have a certain level of dissemination, i.e., to communicate the results of our research to the public.
Most of our dissemination activities involve publishing academic papers, which ideally should be open-access, holding presentations and talks at both public seminars and for interested companies, and writing popular science publications.
I had an idea to

- **Federated learning**: I'm looking into using the federated learning framework [Flower](https://flower.dev/) for some of our projects. We did a survey on various existing frameworks
- **Green AI**: We are spending a lot of time researching methods for making AI more sustainable.
