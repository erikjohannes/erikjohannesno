---
title: "Gjert the border collie doing what he loves most"
date: 2022-08-01T07:17:11+01:00
type: ["posts"]
draft: false
tags:
    - dogs
categories:
    - agility
mastodonurl: ""
twitterurl: ""
instagramurl: ""
pixelfedurl: ""
---

I shot a short video of Gjert the border collie during one of his agility
workouts. I am using the GoPro HERO7 Black, with no gimbal, and I must say I am
impressed with the HyperSmooth video stabilisation. 

Take a look!

{{< rawhtml >}}
<video id="video" width="1280" height="360" controls>
    <source src="posts/20220801-agility/agility-720.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video> 
{{< /rawhtml >}}
