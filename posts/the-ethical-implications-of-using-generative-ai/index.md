---
title: "The ethical implications of using generative AI"
date: 2021-05-24T21:47:12+02:00
type: ["posts"]
draft: true
tags:
categories:
---

There are several issues regarding the use of the large generative AI models which are

- **Theft of art and copyrighted content**.
- **Tremendous carbon footprint and resources usage**.
- **Bad working conditions**.
