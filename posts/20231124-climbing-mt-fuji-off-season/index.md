---
title: "Sunshine and solitude on Mt Fuji"
date: 2023-11-24T11:40:21+01:00
draft: false
---

<!-- However, Mount Fuji, Japan's highest peak, was not too far from Tokyo, where my flights would arrive and depart. -->
<!-- I only had two and a half days after the conference, and I wanted to spend the last day in Tokyo, leaving practically only one day to spend on climbing/hiking. -->

I was fortunate enough to get the opportunity for a travel to Japan for a work-related conference earlier in November.
Since I've never been to Japan, and most likely won't go there again, I wanted to stay a few extra days for a small vacation.
My main goal was to see some of the Japanese nature, and ideally visit some mountains.
I researched the mountain areas of Honshu (the largest island of Japan), which are often referred to as the Japanese Alps, but going there would require spending too much of my precious time on trains and buses.
However, Mount Fuji, Japan's highest peak, was not too far from Tokyo, where my flights would arrive and depart.
When I only had time to climb one mountain, the tallest one is a natural choice.

While Mount Fuji is visited by large amounts of people during the summer, it's a different story in the so-called off-season.
The authorities go so far as to close all the trails, and based on the information I found it is required to submit a form to the police in order to request permission to scale the mountain.
Many websites portrayes an off-season climb as extremely dangerous, and disencourages anyone to attempt it.

I downloaded the form, where it is required to provide information about all the members of the expedition, what gear you have, and how much food you are bringing.
I felt the chances of being given permission for a one-man "expedition" in a one-day push was very low.
It didn't really make sense that they would be this strict for such a non-technical mountain, but I can understand that the authorities want to avoid unnecessary rescue operations.
I had a feeling that it should be possible to climb the mountain without too much fuzz.
I found a guy through social media who had been to the summit earlier this fall, who confirmed this.
He had no problem getting access to the mountain, and the climb was not as demanding as they make it seem.
Crampons and ice axe was necessary, but otherwise it should be fine.
I packed the suggested ice gear, along with shell clothing, down jacket and an emergency shelter.
I was unfamiliar with the weather and climate in the region, and wanted to be prepared for cold temperatures and wind.

When I arrived in Japan, the weather did not look good for the day I had planned to summit.
Up until the morning of summit day I had actually decided to skip the climb, and rather focus on seeing more of the Japanese culture.
The whole mountain was completely covered in clouds, and I didn't want to spend my very limited time in Japan walking in fog, not seeing anything, and most likely have to turn around before the summit.
I started the day with a run past the Arakura Shengen Shrine, and up into the forest-covered mountains around Fujiyoshida, where I was staying.
However, the desire to ascend a real mountain became too strong, and I changed my mind.
I went back to the hotel, packed my gear, and ran to Kawaguchiko, where I jumped on the bus to Fuji-Subaru 5th Station.
The 5th Station is the most common place to start hiking in the summer.
According to what I read online, the bus didn't operate past October, but evidently that was wrong.

When we approached the final stop at 2300 masl I realized that the clouds were quite low, and above this altitude the sun was shining.
After a brief look in the souvenir shops, I made my way towards the trail, where you have to pass a large barrier with signs informing you that the trail is closed for the season.
Another barrier is further down the trail, just before the path starts ascending up the mountain.

![Signs indicating the closing of trails for the off-season. The orange barrier can be seen in the background.](posts/20231124-climbing-mt-fuji-off-season/01.jpeg)
*Signs indicating the closing of trails for the off-season. The orange barrier can be seen in the background.*

The first part of the trail is what I would call a volcanic gravel road.
After a while it gets more rocky, and you're walking on a proper "trail".
The path is fenced by chains and ropes almost the whole way up, so it's virtually impossible to get lost. 
At about 3000 masl I had to put on crampons.
There was not a large amount of snow, but the path and rocks surrounding it was covered in relatively hard ice that would have made it impossible to move upwards without spikes.
The thinner air was getting noticeable, and I had to move slower than normal, with my breathing getting heavier.

![At the bottom of the trail.](posts/20231124-climbing-mt-fuji-off-season/02.jpeg)
*At the bottom of the trail.*

Finally up at the crater rim, I could enjoy a fantastic view of both the crater itself, and the never-ending sea of clouds that surrounded the mountain.
At the opposite side of the rim was my final destination: the highest point of the mountain, Kengamine.
The wind was quite strong here, so I often had to lean into the wind in order to move forward.
As the peakbagger I am, I walked over all the small peaks surrounding the crater on my way to the highest point.

![Cloudy weather looks different from above.](posts/20231124-climbing-mt-fuji-off-season/03.jpeg)
*Cloudy weather looks different from above.*

A weather station is placed on top of Kengamine, and here the wind was particularly strong.
I snapped some photos, and walked a bit down again before I took a break to eat some snacks and enjoy the view.
It was a fantastic feeling to experience Mount Fuji towering over the clouds, sun shining, and having the whole mountain for myself (as far as I could see at least, and I saw no other tracks in the snow and ice).
And to think that I almost decided not to go!

![View of the crater from Kengamine.](posts/20231124-climbing-mt-fuji-off-season/04.jpeg)
*View of the crater from Kengamine.*

<!-- ![Standing on top of the weather station.](posts/20231124-climbing-mt-fuji-off-season/05.jpeg) -->
<!-- *Standing on top of the weather station.* -->

I continued to follow the rim back to where I had come up, completing the crater circuit.
At one point I fell down a rocky slope, heading towards the top of a small cliff.
A sudden, forceful gust of wind pushed me off my feet, and one of my crampons got caught in the ice, preventing my from finding my footing.
I tumbled down the icy rocks, going head over heels a couple of times before I manage to stop myself.
Luckily I only scraped my knee.
<!-- , but it was a useful reminder of the small margin of error in the mountains.-->

![Enjoying the views.](posts/20231124-climbing-mt-fuji-off-season/06.jpeg)
*Enjoying the views.*

From the crater rim I moved effectively down again, jogging and running.
Further down, I met a couple of guys who had had to turn around because they didn’t have crampons.
Looking at the watch, I saw that I had time to catch the last bus down from 5th Station, but I decided to go down the complete Yoshida Trail, down to my hotel in Fujiyoshida.
I had already spent too much time sitting on buses and trains on this trip, and I wanted to see more of the Japanese nature.

![The way down.](posts/20231124-climbing-mt-fuji-off-season/07.jpeg)
*The way down.*

At about 2400 masl the trail descends into the forest.
The mist lay thick between the trees, and there was an almost eerie atmosphere.
There are several ruins of old pilgrim logdings and shrines along the path, with information signs about the rich history of the Fuji mountain.

When the trail passed into the flatlands between the mountain and the city, signs started appearing about bears in the area.
I didn't think too much about it, but suddenly I saw movement in the woods ahead of me.
About fity meters away, the dark shape of a bear ran straight across my path and disappeared among the trees to my left.
I had been running in order to get back before it got dark, but now I made a sudden stop.
I felt a slight regret that I didn't buy one of the "bear bells" that every hiker in Japan wears, but I’m not sure how effective the actually are.
Slowly, I walked past where the bear had appeared, and talked out loud as a substitue for the bear bell.
I hadn't made any effort to find out if these bears are aggresive and how one should begave in the vicinity of one.
After a couple of hundred meters I decided that I was safe, and I resumed running towards Fujiyoshida.
I concluded my trip at the Kitaguchi Hongu Fuji Sengen Shrine, before I jogged back to the hotel and ate dinner.

![Kitaguchi Hongu Fuji Shengen Shrine, the official starting point of the Yoshida Trail.](posts/20231124-climbing-mt-fuji-off-season/08.jpeg)
*Kitaguchi Hongu Fuji Shengen Shrine, the official starting point of the Yoshida Trail.*

My trip to Mount Fuji was a wonderful experience, and I feel lucky to have experienced it in sunshine and solitude.
To be clear: Climbing Mount Fuji in the off-season requires experience, and being in the mountains always entails risks, especially with regard to weather and changing conditions.
Having the correct clothes and gear, knowing how to use it and knowing your own limits is essential.
However, the mountain is definitely accessible for experienced hikers when the weather and conditions allow it.








