<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name="author" content="Erik Johannes Husom" />
        <meta name="description" content="Personal website." />
        <title>Dotfiles: Starting over after eight years
 – Erik Johannes Husom</title>
        <base href="https://erikjohannes.no/">
        <!-- <base href="file:///Users/erikjohannes/Documents/erikjohannesno/"> -->
        <!-- <base href="file:///home/erikhu/Documents/erikjohannesno/"> -->
        
        <link rel="icon" href="data:,">
        <link href="style.css" rel="stylesheet" type="text/css" title="Stylesheet">
        <link rel="alternate" type="application/rss+xml" href="/index.xml" title="RSS Feed">
    </head>
    <body>
    <header>
        <br>
        <nav class=header-nav>
            <a href="index.html"><h1 class=p-name rel="me">Erik Johannes Husom</h1></a>
        </nav>
    </header>
<article>
<h2>Dotfiles: Starting over after eight years
</h2>
<time>03 Oct 2024</time><p>A couple of months ago I decided it was time for a digital deep clean of my laptop.
It was close to three years since last time, and over that period a lot of unnecessary software and data has aggregated in the nooks and corners of the system.
The easiest way to achieve a clean system is to reinstall the operating system, which also gave me a chance to upgrade Ubuntu from 22.04 to 24.04.</p>
<p>When setting up a new system, one of the first things you want to do is to install and configure your most used programs.
As many other programmers and Linux users, I have a <code>dotfiles</code> folder that contain my precious configuration files for all the vital utilities I use.</p>
<p>For me, the most important ones are <code>bash</code>, <code>vim</code> and <code>tmux</code>.</p>
<!-- I'll only focus on the dotfiles for these three programs in this post. -->

<h3>Time for a clean slate</h3>
<p>It's now eight years since I entered a computer terminal<sup id="fnref:1"><a class="footnote-ref" href="posts/20241003-a-clean-slate-for-dotfiles/index.html#fn:1">1</a></sup> for the first time, and over that period a lot of things have accumulated in my dotfiles.
I have been using different Linux distros, macOS, and Windows Subsystem for Linux, and have made my configuration adaptable to several different setups.</p>
<p>At the same time, I like to think of myself as a minimalist, in some sense. I like to keep my system as fast and uncluttered as possible. I'm not sure if my dotfiles reflect this, but I know I'm quite restrictive compared to some "vim maximalists".</p>
<p>My <code>.vimrc</code> has grown to ~500 lines, which is excluding the vimscripts, plugins and templates I use. 
I have a <code>.bashrc</code> of ~400 lines, and a <code>.tmux.conf</code> of ~60 lines.</p>
<p>In addition to that, I have 65 bash scripts, counting ~5500 lines in total.
These are not really configuration files, but I include them in my dotfiles as a part of my bash setup.</p>
<p>My dotfiles are by no means bloated compared to other software I use regularly, but I wondered: How much of this I actually need?</p>
<p>With a newly installed operating system, I thought it would be an excellent opportunity to start from scratch, and only add back things I actually felt the need for.</p>
<p>Now, after about 7 weeks since the clean install, I think it's time to assess the status.
What parts of my dotfiles did actually make it on to the next phase?</p>
<h3>Bash</h3>
<p>In bash, I added back some lines quite quickly, mainly history settings and aliases. I use many shortcuts all the time, and they are firmly etched into my muscle memory:</p>
<details>
<summary>Expand to see .bashrc (53 lines)</summary>

<pre><code class="language-bash"># Use vi keybindings on command line
set -o vi

# Prompt appearance
export PS1=&quot;\[\e[31m\]\u\[\e[m\]@\[\e[32m\]\h\[\e[m\] \[\e[34m\]\w\[\e[m\] \\$ &quot;

# Adjust history size
HISTSIZE=20000
HISTFILESIZE=20000
SAVEHIST=20000
# Ignore duplicate entries in the history
HISTCONTROL=ignoredups:erasedups
# The following lines are needed to save history from multiple sessions
# append history entries..
shopt -s histappend
# After each command, save and reload history
PROMPT_COMMAND=&quot;history -a; history -c; history -r; $PROMPT_COMMAND&quot;


# Aliases

alias x='exit'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias v='vim .'
alias ll='ls -AhlF --color=auto'  # human-readable file size
alias la='ls -A'
alias l='ls -CF --color=auto'
alias duh='du -h -d 1'                    # Show file human-readable file sizes
alias df='df -h'                          # Show disk usage with human-readable sizes
alias cp=&quot;cp -i&quot;                          # confirm before overwriting something
alias grepr=&quot;grep -rn . -e&quot;

alias g='git status'
alias ga='git add'
alias gc='git commit'
alias gcm='git commit -m'
alias gd='git diff'
alias gp='git push'
alias gpl='git pull'
alias gl='git log --graph --oneline' # show git log as graph
alias gcred='git config credential.helper store'
alias gitmail='git config user.email'
alias gitname='git config user.name'
alias gclone='git clone'
alias gall=&quot;find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} &amp;&amp; \
    cd {} &amp;&amp; git status -s &amp;&amp; echo)' \;&quot; # check git status of all subfolders
# git pull on all subfolders:
alias gplall=&quot;find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} &amp;&amp; \
    cd {} &amp;&amp; git pull &amp;&amp; echo)' \;&quot;
</code></pre>

</details>

<p>The configuration file itself was reduced down to about 25% of its previous size, but the biggest difference is for the bash scripts: Out of 65, <em>none</em> have made their over to my freshly installed system. </p>
<p>All the time I spent writing scripts to automate certain actions seems to have gone to waste (although I probably learned a lot in the process). 
I guess I miscalculated how much I would actually use those scripts in my daily work.</p>
<h3>Tmux</h3>
<p>For tmux, I needed about half of my original configuration:</p>
<pre><code># Remap prefix to Control + a
set -g prefix C-a
# Bind 'C-a C-a' to type 'C-a', making sure 'C-a' works in other tools
bind C-a send-prefix
# Remove original prefix binding
unbind C-b

# Set vi mode
setw -g mode-keys vi

# Move to different windows using vim keybindings (hjkl) instead of arrow keys
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R

# Name windows after directory
set -g window-status-format '#I:#(pwd=&quot;#{pane_current_path}&quot;; echo ${pwd####*/} | cut -c1-20)#F'
set -g window-status-current-format '#I:#(pwd=&quot;#{pane_current_path}&quot;; echo ${pwd####*/} | cut -c1-20)#F'

# Open new windows and panes in same directory as active
bind c new-window -c &quot;#{pane_current_path}&quot;
bind '&quot;' split-window -c &quot;#{pane_current_path}&quot;
bind % split-window -h -c &quot;#{pane_current_path}&quot;
</code></pre>
<p>I didn't think my tmux-configuration was that important, but when you work mostly in the terminal, it's important to be able to navigate with ease.</p>
<h3>Vim</h3>
<p>My setup has reduced drastically for vim.
Going from ~500 lines, 7 vimscripts, and 3 plugins, I'm now down to 9 lines and a single script:</p>
<pre><code class="language-vim">set expandtab       &quot; Use spaces instead of tabs
set tabstop=4       &quot; Number of spaces a tab counts for
set shiftwidth=4    &quot; Number of spaces to use for each level of indentation
set softtabstop=4   &quot; Number of spaces that a tab counts for while editing
set autoindent      &quot; Keep the same indentation as the current line
set smartindent     &quot; Automatically add extra indent in certain contexts (like after `{` in C-like languages)
set number          &quot; Show line numbers
filetype indent plugin on   &quot; determine file type and use auto-indenting
source ~/.vim/vimscripts/commentary.vim &quot; Comment/uncomment with gc[motion]. Credit: Tim Pope &lt;http://tpo.pe/&gt;
</code></pre>
<p>I currently don't feel the need to add any plugins to vim.
Previously I used <code>fzf</code><sup id="fnref:2"><a class="footnote-ref" href="posts/20241003-a-clean-slate-for-dotfiles/index.html#fn:2">2</a></sup> quite a lot, both in bash and vim. It is an awesome tool, and it felt like I supercharged my efficiency in the terminal when I started using it.</p>
<p>Now, I realize that the gains were not as large as I thought, and that can do my work efficiently without fuzzy search.
I can easily navigate and find what I need without relying on a complex integration of <code>fzf</code>, <code>bash</code>, <code>vim</code>, <code>find</code>, <code>bat</code>, <code>grep</code>, and <code>ripgrep</code>, which is what I had before (and spent a long time on setting up).
Maybe one day I'll be back there, but not until I feel the need for it.</p>
<h3>Conclusion</h3>
<p>All in all, I love how these tools are designed to be easily configured, adapted, and integrated with eachother.
It's how all software should be designed: Giving people power over the tools they use.</p>
<p>But I also think it's important to step back once in a while and think about how we use the tools, and what tools we actually need.</p>
<p>It is a good reminder to observe that what I once thought of as a large improvement, actually did not bring as much benefit as I thought.
Maybe that applies to other things in life as well.</p>
<div class="footnote">
<hr />
<ol>
<li id="fn:1">
<p>Technically a <em>terminal emulator</em>.&#160;<a class="footnote-backref" href="posts/20241003-a-clean-slate-for-dotfiles/index.html#fnref:1" title="Jump back to footnote 1 in the text">&#8617;</a></p>
</li>
<li id="fn:2">
<p><a href="https://github.com/junegunn/fzf">https://github.com/junegunn/fzf</a>&#160;<a class="footnote-backref" href="posts/20241003-a-clean-slate-for-dotfiles/index.html#fnref:2" title="Jump back to footnote 2 in the text">&#8617;</a></p>
</li>
</ol>
</div></article>
    <footer>
        Reply via <a href="mailto:erikjohannes@protonmail.com">email</a>.
        Subscribe with <a href="index.xml">RSS</a>.
        <br />
		No tracking. No cookies. No visit logs.
        <br />
        &copy;2018-2025 Erik Johannes Husom.
    </footer>
    </body>
</html>