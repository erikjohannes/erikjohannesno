---
title: "Dotfiles: Starting over after eight years"
date: 2024-10-03T07:58
type: ["posts"]
draft: false
tags:
    - programming
    - linux
    - bash
    - vim
    - dotfiles
    - technology
    - minimalism
categories:
---

A couple of months ago I decided it was time for a digital deep clean of my laptop.
It was close to three years since last time, and over that period a lot of unnecessary software and data has aggregated in the nooks and corners of the system.
The easiest way to achieve a clean system is to reinstall the operating system, which also gave me a chance to upgrade Ubuntu from 22.04 to 24.04.

When setting up a new system, one of the first things you want to do is to install and configure your most used programs.
As many other programmers and Linux users, I have a `dotfiles` folder that contain my precious configuration files for all the vital utilities I use.

For me, the most important ones are `bash`, `vim` and `tmux`.
<!-- I'll only focus on the dotfiles for these three programs in this post. -->

### Time for a clean slate

It's now eight years since I entered a computer terminal[^1] for the first time, and over that period a lot of things have accumulated in my dotfiles.
I have been using different Linux distros, macOS, and Windows Subsystem for Linux, and have made my configuration adaptable to several different setups.

At the same time, I like to think of myself as a minimalist, in some sense. I like to keep my system as fast and uncluttered as possible. I'm not sure if my dotfiles reflect this, but I know I'm quite restrictive compared to some "vim maximalists".

My `.vimrc` has grown to ~500 lines, which is excluding the vimscripts, plugins and templates I use. 
I have a `.bashrc` of ~400 lines, and a `.tmux.conf` of ~60 lines.

In addition to that, I have 65 bash scripts, counting ~5500 lines in total.
These are not really configuration files, but I include them in my dotfiles as a part of my bash setup.

My dotfiles are by no means bloated compared to other software I use regularly, but I wondered: How much of this I actually need?

With a newly installed operating system, I thought it would be an excellent opportunity to start from scratch, and only add back things I actually felt the need for.

Now, after about 7 weeks since the clean install, I think it's time to assess the status.
What parts of my dotfiles did actually make it on to the next phase?

### Bash

In bash, I added back some lines quite quickly, mainly history settings and aliases. I use many shortcuts all the time, and they are firmly etched into my muscle memory:

<details>
<summary>Expand to see .bashrc (53 lines)</summary>
```bash
# Use vi keybindings on command line
set -o vi

# Prompt appearance
export PS1="\[\e[31m\]\u\[\e[m\]@\[\e[32m\]\h\[\e[m\] \[\e[34m\]\w\[\e[m\] \\$ "

# Adjust history size
HISTSIZE=20000
HISTFILESIZE=20000
SAVEHIST=20000
# Ignore duplicate entries in the history
HISTCONTROL=ignoredups:erasedups
# The following lines are needed to save history from multiple sessions
# append history entries..
shopt -s histappend
# After each command, save and reload history
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"


# Aliases

alias x='exit'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias v='vim .'
alias ll='ls -AhlF --color=auto'  # human-readable file size
alias la='ls -A'
alias l='ls -CF --color=auto'
alias duh='du -h -d 1'                    # Show file human-readable file sizes
alias df='df -h'                          # Show disk usage with human-readable sizes
alias cp="cp -i"                          # confirm before overwriting something
alias grepr="grep -rn . -e"

alias g='git status'
alias ga='git add'
alias gc='git commit'
alias gcm='git commit -m'
alias gd='git diff'
alias gp='git push'
alias gpl='git pull'
alias gl='git log --graph --oneline' # show git log as graph
alias gcred='git config credential.helper store'
alias gitmail='git config user.email'
alias gitname='git config user.name'
alias gclone='git clone'
alias gall="find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && \
    cd {} && git status -s && echo)' \;" # check git status of all subfolders
# git pull on all subfolders:
alias gplall="find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && \
    cd {} && git pull && echo)' \;"
```
</details>

The configuration file itself was reduced down to about 25% of its previous size, but the biggest difference is for the bash scripts: Out of 65, *none* have made their over to my freshly installed system. 

All the time I spent writing scripts to automate certain actions seems to have gone to waste (although I probably learned a lot in the process). 
I guess I miscalculated how much I would actually use those scripts in my daily work.

### Tmux

For tmux, I needed about half of my original configuration:

```
# Remap prefix to Control + a
set -g prefix C-a
# Bind 'C-a C-a' to type 'C-a', making sure 'C-a' works in other tools
bind C-a send-prefix
# Remove original prefix binding
unbind C-b

# Set vi mode
setw -g mode-keys vi

# Move to different windows using vim keybindings (hjkl) instead of arrow keys
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R

# Name windows after directory
set -g window-status-format '#I:#(pwd="#{pane_current_path}"; echo ${pwd####*/} | cut -c1-20)#F'
set -g window-status-current-format '#I:#(pwd="#{pane_current_path}"; echo ${pwd####*/} | cut -c1-20)#F'

# Open new windows and panes in same directory as active
bind c new-window -c "#{pane_current_path}"
bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
```

I didn't think my tmux-configuration was that important, but when you work mostly in the terminal, it's important to be able to navigate with ease.

### Vim

My setup has reduced drastically for vim.
Going from ~500 lines, 7 vimscripts, and 3 plugins, I'm now down to 9 lines and a single script:

```vim
set expandtab       " Use spaces instead of tabs
set tabstop=4       " Number of spaces a tab counts for
set shiftwidth=4    " Number of spaces to use for each level of indentation
set softtabstop=4   " Number of spaces that a tab counts for while editing
set autoindent      " Keep the same indentation as the current line
set smartindent     " Automatically add extra indent in certain contexts (like after `{` in C-like languages)
set number          " Show line numbers
filetype indent plugin on   " determine file type and use auto-indenting
source ~/.vim/vimscripts/commentary.vim " Comment/uncomment with gc[motion]. Credit: Tim Pope <http://tpo.pe/>
```

I currently don't feel the need to add any plugins to vim.
Previously I used `fzf`[^2] quite a lot, both in bash and vim. It is an awesome tool, and it felt like I supercharged my efficiency in the terminal when I started using it.

Now, I realize that the gains were not as large as I thought, and that can do my work efficiently without fuzzy search.
I can easily navigate and find what I need without relying on a complex integration of `fzf`, `bash`, `vim`, `find`, `bat`, `grep`, and `ripgrep`, which is what I had before (and spent a long time on setting up).
Maybe one day I'll be back there, but not until I feel the need for it.

### Conclusion

All in all, I love how these tools are designed to be easily configured, adapted, and integrated with eachother.
It's how all software should be designed: Giving people power over the tools they use.

But I also think it's important to step back once in a while and think about how we use the tools, and what tools we actually need.

It is a good reminder to observe that what I once thought of as a large improvement, actually did not bring as much benefit as I thought.
Maybe that applies to other things in life as well.

[^1]: Technically a *terminal emulator*.
[^2]: [https://github.com/junegunn/fzf](https://github.com/junegunn/fzf)
