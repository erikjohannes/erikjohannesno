---
title: "The Tech Challenge"
date: 2023-06-10
type: ["posts"]
draft: true
tags:
categories:
---


A question popped into my head today: Is i

Being to idealistic leads to a lot of disappointment and frustration, and can also hinder actual progress by setting standards too high.

When we introduce new technology,
p

Big tech firms are able to deploy new software very fast on devices all across the world

When we introduce new technology, and especiialy on a large scale, we cannot be sure how it will affect us.
We can hypothesize and try to extrapolate from historical events, but humans are in general bad at predicting the future[citation needed].

Those who are most concerned with avoiding bad consequences have no evidence to refer to when claiming that a certain technology might have severe negative effects, and the optimists can always 

Policy-makers are usually too slow to act in time, which is of course a consequence of our democratic system.
This 


In response to new technology, we are usually reactive, rather than proactive.
The real effects of changes is usually seen after several years, and we need scientific studies to really confirm what the effects are.
Sadly, this is not enough to


Ethics needs to be high on the agenda for technology and software developers.
The scale

### Is there a limit to how much technology we should accept in our lives?




Above all, I think the most important thing is to keep the discussion going, and taking into account all perspectives.
From personal experience I can say that it can be a bit disheartening to see how much both experts and laymen disagree about any and every aspect related to the recent developments in technology (and AI especially), and sometimes it seems that there can be no way forward without 
