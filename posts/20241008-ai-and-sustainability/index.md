---
title: "AI's sustainability issues are gaining awareness"
date: 2024-10-08T07:58
type: ["posts"]
draft: false
tags:
    - artificialintelligence
    - ai
    - sustainability
    - greenai
    - climatechange
    - energyconsumption
    - microsoft
    - google
    - bigtech
    - news
categories:
---

I'm happy to see that there is an increasing focus on the challenge regarding AI and sustainability.
Not only because I'm a researcher trying to make AI "greener", but because AI's booming resource consumption is starting to become a real problem.
The last few months I have seen many articles bring up some of the issues, and I would like to highlight a few of them.

### Microsoft playing both sides

The Atlantic writes about "Microsoft's hypocrisy on AI"[^1], describing how Microsoft claims to take responsibility for climate action, while still having large scale cooperation with fossil-fuel companies.
While the article's focus is mainly on the application of AI, I think the following observation about AI's own climate footprint is important:

> The idea that AI’s climate benefits will outpace its environmental costs is largely speculative, however, especially given that generative-AI tools are themselves tremendously resource-hungry. Within the next six years, the data centers required to develop and run the kinds of next-generation AI models that Microsoft is investing in may use more power than all of India. They will be cooled by millions upon millions of gallons of water. All the while, scientists agree, the world will get warmer, its climate more extreme.

There are many possible ways in which we can use AI for limiting carbon emissions, but widely deployed chatbots and image generators are not among them.
We have to stop generalizing the benefits of one AI application to support any type of AI usage.

The article is based partly on an interview with Holly Alpine, previously Microsoft's Sr. Program Manager of Datacenter Community Environmental Sustainability.
She has written her own account on why she left the company[^2], which gives additional insight to the problem:

> Internally, discussions highlighted the most significant application of generative AI for the oil and gas industry: optimizing exploration activities. AI was celebrated as a “game changer” and the key for the fossil fuel industry to continue to be competitive.

Again, AI can be used to fight climate change, but it certainly can be used to hasten it as well.

### Big Tech's emissions are surging

There is no doubt that the enormous investment in AI, and particularly generative AI, is driving energy consumption and emissions up for many companies.
NPR reports that both Google and Microsoft have significantly increased carbon emissions[^3].
Google's emissions are up by 48% since 2019, and Microsoft's have increased by 29% since 2020.

This is despite the fact that both companies have pledged to be carbon-neutral by 2030.
In both cases, the need for scaling up infrastructure to support AI is given as a reason for why emissions are soaring.

### Generative AI and energy-efficiency

I was lucky enough to be interviewed about the energy consumption of large language models in the online news magazine Heatmap News[^4].
Slightly less known than The Atlantic and NPR, but even so, I cherish every opportunity to talk about my research in more public channels.

I tried to drive home the point that generative AI is in general very expensive, and that we need to think hard about when and where it is actually worth using it.
It was mainly the first point that made it through to the final article, but the second one is just as important.

I commented specifically on the new o1-models from OpenAI, which use so-called "chain-of-thought" to increase their performance in problem solving.
While the effort to reduce the hallucinations of LLMs is laudable, the new models are almost certainly much more expensive than previous models.
To quote myself:

> As Erik Johannes Husom, a researcher at SINTEF Digital, a Norwegian research organization, told me, “It looks like we’re going to get another acceleration of generative AI’s carbon footprint.”

Let's hope we have some breakthroughs soon to make models more efficient, not more resource-demanding.

### Will "greener" AI help?

There is an underlying challenge with working towards making AI (or any type of technology) more energy-efficiency: The cheaper it gets to use something, the more likely we are to use more of it.

I'm worried that our efforts to make AI "greener" will only lead to more unecessary consumption.

Nevertheless, increased awareness is a good beginning for making a change.

[^1]: Hao, K. (2024, September 13). Microsoft’s hypocrisy on AI. *The Atlantic*. [https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/](https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/). Archive.org: [https://web.archive.org/web/20240913122346/https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/](https://web.archive.org/web/20240913122346/https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/).
[^2]: Alpine, H. (2024, September 18). I loved my job at Microsoft, but I had to resign on principle. Here’s why. *Fortune*. [https://fortune.com/2024/09/17/microsoft-environment-oil-fossil-fuel-expansion-climate-change/](https://fortune.com/2024/09/17/microsoft-environment-oil-fossil-fuel-expansion-climate-change/).
[^3]: Kerr, D. (2024, July 12). AI brings soaring emissions for Google and Microsoft, a major contributor to climate change. *NPR*. [https://www.npr.org/2024/07/12/g-s1-9545/ai-brings-soaring-emissions-for-google-and-microsoft-a-major-contributor-to-climate-change](https://www.npr.org/2024/07/12/g-s1-9545/ai-brings-soaring-emissions-for-google-and-microsoft-a-major-contributor-to-climate-change).
[^4]: Zeitlin, M. (2024, September 17). What Does OpenAI’s New Breakthrough Mean for Energy Consumption? *Heatmap News*. [https://heatmap.news/technology/openai-o1-energy](https://heatmap.news/technology/openai-o1-energy). Archive.org: [https://web.archive.org/web/20240917231307/https://heatmap.news/technology/openai-o1-energy](https://web.archive.org/web/20240917231307/https://heatmap.news/technology/openai-o1-energy).
