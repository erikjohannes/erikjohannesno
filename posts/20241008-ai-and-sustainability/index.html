<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name="author" content="Erik Johannes Husom" />
        <meta name="description" content="Personal website." />
        <title>AI's sustainability issues are gaining awareness
 – Erik Johannes Husom</title>
        <base href="https://erikjohannes.no/">
        <!-- <base href="file:///Users/erikjohannes/Documents/erikjohannesno/"> -->
        <!-- <base href="file:///home/erikhu/Documents/erikjohannesno/"> -->
        
        <link rel="icon" href="data:,">
        <link href="style.css" rel="stylesheet" type="text/css" title="Stylesheet">
        <link rel="alternate" type="application/rss+xml" href="/index.xml" title="RSS Feed">
    </head>
    <body>
    <header>
        <br>
        <nav class=header-nav>
            <a href="index.html"><h1 class=p-name rel="me">Erik Johannes Husom</h1></a>
        </nav>
    </header>
<article>
<h2>AI's sustainability issues are gaining awareness
</h2>
<time>08 Oct 2024</time><p>I'm happy to see that there is an increasing focus on the challenge regarding AI and sustainability.
Not only because I'm a researcher trying to make AI "greener", but because AI's booming resource consumption is starting to become a real problem.
The last few months I have seen many articles bring up some of the issues, and I would like to highlight a few of them.</p>
<h3>Microsoft playing both sides</h3>
<p>The Atlantic writes about "Microsoft's hypocrisy on AI"<sup id="fnref:1"><a class="footnote-ref" href="posts/20241008-ai-and-sustainability/index.html#fn:1">1</a></sup>, describing how Microsoft claims to take responsibility for climate action, while still having large scale cooperation with fossil-fuel companies.
While the article's focus is mainly on the application of AI, I think the following observation about AI's own climate footprint is important:</p>
<blockquote>
<p>The idea that AI’s climate benefits will outpace its environmental costs is largely speculative, however, especially given that generative-AI tools are themselves tremendously resource-hungry. Within the next six years, the data centers required to develop and run the kinds of next-generation AI models that Microsoft is investing in may use more power than all of India. They will be cooled by millions upon millions of gallons of water. All the while, scientists agree, the world will get warmer, its climate more extreme.</p>
</blockquote>
<p>There are many possible ways in which we can use AI for limiting carbon emissions, but widely deployed chatbots and image generators are not among them.
We have to stop generalizing the benefits of one AI application to support any type of AI usage.</p>
<p>The article is based partly on an interview with Holly Alpine, previously Microsoft's Sr. Program Manager of Datacenter Community Environmental Sustainability.
She has written her own account on why she left the company<sup id="fnref:2"><a class="footnote-ref" href="posts/20241008-ai-and-sustainability/index.html#fn:2">2</a></sup>, which gives additional insight to the problem:</p>
<blockquote>
<p>Internally, discussions highlighted the most significant application of generative AI for the oil and gas industry: optimizing exploration activities. AI was celebrated as a “game changer” and the key for the fossil fuel industry to continue to be competitive.</p>
</blockquote>
<p>Again, AI can be used to fight climate change, but it certainly can be used to hasten it as well.</p>
<h3>Big Tech's emissions are surging</h3>
<p>There is no doubt that the enormous investment in AI, and particularly generative AI, is driving energy consumption and emissions up for many companies.
NPR reports that both Google and Microsoft have significantly increased carbon emissions<sup id="fnref:3"><a class="footnote-ref" href="posts/20241008-ai-and-sustainability/index.html#fn:3">3</a></sup>.
Google's emissions are up by 48% since 2019, and Microsoft's have increased by 29% since 2020.</p>
<p>This is despite the fact that both companies have pledged to be carbon-neutral by 2030.
In both cases, the need for scaling up infrastructure to support AI is given as a reason for why emissions are soaring.</p>
<h3>Generative AI and energy-efficiency</h3>
<p>I was lucky enough to be interviewed about the energy consumption of large language models in the online news magazine Heatmap News<sup id="fnref:4"><a class="footnote-ref" href="posts/20241008-ai-and-sustainability/index.html#fn:4">4</a></sup>.
Slightly less known than The Atlantic and NPR, but even so, I cherish every opportunity to talk about my research in more public channels.</p>
<p>I tried to drive home the point that generative AI is in general very expensive, and that we need to think hard about when and where it is actually worth using it.
It was mainly the first point that made it through to the final article, but the second one is just as important.</p>
<p>I commented specifically on the new o1-models from OpenAI, which use so-called "chain-of-thought" to increase their performance in problem solving.
While the effort to reduce the hallucinations of LLMs is laudable, the new models are almost certainly much more expensive than previous models.
To quote myself:</p>
<blockquote>
<p>As Erik Johannes Husom, a researcher at SINTEF Digital, a Norwegian research organization, told me, “It looks like we’re going to get another acceleration of generative AI’s carbon footprint.”</p>
</blockquote>
<p>Let's hope we have some breakthroughs soon to make models more efficient, not more resource-demanding.</p>
<h3>Will "greener" AI help?</h3>
<p>There is an underlying challenge with working towards making AI (or any type of technology) more energy-efficiency: The cheaper it gets to use something, the more likely we are to use more of it.</p>
<p>I'm worried that our efforts to make AI "greener" will only lead to more unecessary consumption.</p>
<p>Nevertheless, increased awareness is a good beginning for making a change.</p>
<div class="footnote">
<hr />
<ol>
<li id="fn:1">
<p>Hao, K. (2024, September 13). Microsoft’s hypocrisy on AI. <em>The Atlantic</em>. <a href="https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/">https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/</a>. Archive.org: <a href="https://web.archive.org/web/20240913122346/https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/">https://web.archive.org/web/20240913122346/https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/</a>.&#160;<a class="footnote-backref" href="posts/20241008-ai-and-sustainability/index.html#fnref:1" title="Jump back to footnote 1 in the text">&#8617;</a></p>
</li>
<li id="fn:2">
<p>Alpine, H. (2024, September 18). I loved my job at Microsoft, but I had to resign on principle. Here’s why. <em>Fortune</em>. <a href="https://fortune.com/2024/09/17/microsoft-environment-oil-fossil-fuel-expansion-climate-change/">https://fortune.com/2024/09/17/microsoft-environment-oil-fossil-fuel-expansion-climate-change/</a>.&#160;<a class="footnote-backref" href="posts/20241008-ai-and-sustainability/index.html#fnref:2" title="Jump back to footnote 2 in the text">&#8617;</a></p>
</li>
<li id="fn:3">
<p>Kerr, D. (2024, July 12). AI brings soaring emissions for Google and Microsoft, a major contributor to climate change. <em>NPR</em>. <a href="https://www.npr.org/2024/07/12/g-s1-9545/ai-brings-soaring-emissions-for-google-and-microsoft-a-major-contributor-to-climate-change">https://www.npr.org/2024/07/12/g-s1-9545/ai-brings-soaring-emissions-for-google-and-microsoft-a-major-contributor-to-climate-change</a>.&#160;<a class="footnote-backref" href="posts/20241008-ai-and-sustainability/index.html#fnref:3" title="Jump back to footnote 3 in the text">&#8617;</a></p>
</li>
<li id="fn:4">
<p>Zeitlin, M. (2024, September 17). What Does OpenAI’s New Breakthrough Mean for Energy Consumption? <em>Heatmap News</em>. <a href="https://heatmap.news/technology/openai-o1-energy">https://heatmap.news/technology/openai-o1-energy</a>. Archive.org: <a href="https://web.archive.org/web/20240917231307/https://heatmap.news/technology/openai-o1-energy">https://web.archive.org/web/20240917231307/https://heatmap.news/technology/openai-o1-energy</a>.&#160;<a class="footnote-backref" href="posts/20241008-ai-and-sustainability/index.html#fnref:4" title="Jump back to footnote 4 in the text">&#8617;</a></p>
</li>
</ol>
</div></article>
    <footer>
        Reply via <a href="mailto:erikjohannes@protonmail.com">email</a>.
        Subscribe with <a href="index.xml">RSS</a>.
        <br />
		No tracking. No cookies. No visit logs.
        <br />
        &copy;2018-2025 Erik Johannes Husom.
    </footer>
    </body>
</html>