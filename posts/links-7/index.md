---
title: "Links #7"
date: 2024-12-28T13:27:23+02:00
type: "posts"
draft: true
---

- [On personal websites and social web](https://manuelmoreale.com/on-personal-websites-and-social-web). 
> On the other you have the people who are realising that maybe the solution is not to recreate social media but rather to abandon it and go back to a more deliberate way to be social online, using personal sites, small forums, emails, and other “traditional” tools. I’m obviously part of this second group.
    - This is an acute observation, and it is related to what I have written earlier about [the future of social media](https://erikjohannes.no/posts/20231119-the-future-of-social-media/). I'm also in the second group. After taking a step back from not only social media, but also online news, I find myself in a much calmer state of mind.
- [FOMO is not a strategy](https://buttondown.com/justenoughinternet/archive/fomo-is-not-a-strategy/)
> There is a small but significant number of people who really like generative AI. This isn’t the same as the number of people who’ve used (or claim to have used) genAI, but a subset of superfans. I tend to meet two kinds of people in this category: enthusiastic senior leaders who have been tasked with doing something innovative but might not be hands-on users, and more junior people who are enterprising and computer-literate and want to try something new. Notably, some of the people in the second group might have a natural affinity for prompt writing but not enjoy another part of their job – like writing long documents or emails –  and so are pleased to outsource those tasks to AI. 
<!-- - [System Administrators' Code of Ethics](https://www.usenix.org/system-administrators-code-ethics) -->

