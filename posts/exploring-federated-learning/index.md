---
title: "Exploring federated learning"
date: 2023-06-10
type: ["posts"]
draft: true
tags:
categories:
---

The last few months I have been exploring the topic of federated learning (FL), which is a way for multiple entities to collaborate on creating a machine learning (ML) model without having to share training data with eachother. This has huge benefits for privacy, and enables the use of ML in domains with a lot of sensitive data, such as the health care domain.

Federated learning is nothing new; it was proposed already in 2016 by researchers from Google (check out the original paper called [Communication-Efficient Learning of Deep Networks from Decentralized Data](https://arxiv.org/abs/1602.05629)).
In this blog post I want to summarize the main aspects of federated learning and my own experiences exploring the current landscape.

<!-- *Side note:* There are already loads of articles on the Internet that explains how federated learning works, so why write another one? -->
<!-- The main goal of this post -->

### What is federated learning, and what are the benefits and challenges?

The simplest way of doing federated learning is to have a global machine learning model that lives on a central server, and this model is then distributed directly to the participants in the training collaboration.
The participants could for example be individuals with their own personal device, or institutions such as hospitals.
Each of the participants trains the model locally on their device, for example a smartphone, which means that the model is updated according to the training data each participant has available.
The updates to the model is sent back from the participants to the server, where all model updates are aggregated and used to update the global model.
There are several different ways of aggregating the model updates, and this may depend on what machine learning algorithm you are using.
The global model is then shared back to the participants.

Using this approach, there is no need for sharing data, but everyone can benefit from the training done by each participant.
Additionally, it has higher bandwidth efficiency, since participants only need to share model updates and not all their training data.

It is important to know that using FL is not a guarantee for 100% privacy, and additional measures may be taken to ensure that no information can be inferred from the shared model updates.
Also, challenges might arise if there are bad actors among the participants.

To summarize the approach, here is federated learning step-by-step (adapted from [flower.dev tutorial on federated learning](https://flower.dev/docs/tutorial/Flower-0-What-is-FL.html):

1. Initialize global model
2. Send global model to clients (devices, organzations, etc.)
3. Each client trains the modal on local data
4. The clients sends model updates to server 
5. Aggregate model updates into the global model
6. Repeat steps 2-5

Let's take a look at different types of federated learning!

### Synchronous vs asynchronous FL

One important aspect of FL is how and when the model updates are aggregated.
Let's first take a look at *when* to aggregate.

- **Synchronous**: All devices required to send their model updates for each global update.
    - **Pros**: Easier to manage and implement. Convergence behavior more predictable.
    - **Cons**: Slower and less scalable. Slow devices can block the process.
- **Aynchronous**: Not dependent on updates from all devices for each iteration.
    - **Pros**: Faster and more scalable. Better handling of "stragglers" (slow devices).
    - **Cons**: More complex to manage.


### Horizontal vs vertical federated learning


### Use cases for federated learning

What can federated learning be used for?

- Mobile keyboard
- Healthcare
- Industri 4.0
- Robotics


### Existing frameworks: A jungle


### The ideal FL framework

In my opinion, the ideal FL framework for using in my research at work should have the following properties:

- Open source software
- Support for multiple FL methods
- Support for use with existing ML libraries

![Aspects of the ideal FL framework.](posts/20230614-exploring-federated-learning/fl-ideal-framework.png)

**Open source software**. This one is a given for most of the tools I use, not only for research but also for personal use.
Using open source tools means that I avoid being dependent on a specific service or distributer of software.
Additionally, for research it is important that the results we produce are reproducible, which is much easier to ensure when using open source software.

**Support for multiple FL methods**: To be usable for a wide range of applications, the framework should offer support for various strategies and variations of federated learning.
In my exploration of FL methods, I think support for asynchronous aggregation is especially important.
If you are dependent on collecting model updates from each and every client for every iteration, it will effectively rule out a lot of use cases where that is impossible to ensure.
In my opinion, supporting vertical FL is not as important as horizontal FL, since the latter is the standard way of doing machine learning (where you always use the same features as input to the model).
Horizontal FL is therefore enough to accomodate federated learning for a wide range of use cases.

**Support for use with existing ML libraries**. This is another aspect that is very important when choosing a framework that you want to use for many different applications.
A framework that can support both TensorFlow models and Scikit-learn models out-of-the-box woujld be very powerful, since you then have access to most traditional ML algorithms.
Implementing support and aggregation strategies for various ML algorithms can be a challenging task

The most promising candidate I have found so far is [Flower](https://flower.dev), and I have started using this in some of our prototypes in a research project I'm working on.


