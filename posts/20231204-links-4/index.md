---
title: "Links #4"
date: 2023-12-04T10:27:23+02:00
type: "posts"
draft: false
---

Another collection of interesting reads from around the web.

- [The Verge: Social media is doomed to die](https://www.theverge.com/2023/4/18/23672769/social-media-inevitable-death-monetization-growth-hacks). Intriguing perspectives from someone who used to work for Snapchat.
- [Baldur Bjarnason: The Elegiac Hindsight of Intelligent Machines](https://softwarecrisis.dev/letters/elegiac-hindsight/). An interesting piece about modern software, AI and ChatGPT.
- [The Fire: Are we ‘Amusing Ourselves to Death’ in 2023?](https://www.thefire.org/news/are-we-amusing-ourselves-death-2023). When you read the book *Amusing ourselves to death* by Neil Postman, originally written about how the television affects our society, it's not difficult to see how all his criticism could be applied to the internet and social media. It would be interesting to know what Postman would have said, had he seen how things have evolved after the advent of the internet.
- [Discourse Magazine: The Psychopathology of Digital Life](https://www.discoursemagazine.com/p/the-psychopathology-of-digital-life). What is the cost of living so much of our lives in the digital world?
- [koray er: Low tech and my old notebook](https://korayer.de/posts/low-tech-and-my-old-notebook.txt). The first paragraph of this post resonates with me:
	> in the past, i've been quite frustrated by unnecessary large-scale technologies. these are technologies that only work because they rely on the advances of modern civilizations, such as electricity or complicated manufacturing processes. the opposite is small-scale or low technology. it's the difference between a vacuum cleaner and a broom, a radiator and a jacket, or maybe even sparkling water and tap water. 
- [Going off-script](https://drewdevault.com/2023/10/13/Going-off-script.html), a blog post that asks some questions regarding our life choices that are worth considering.
- [The Athlantic: My North Star for the Future of AI](https://www.theatlantic.com/technology/archive/2023/11/ai-ethics-academia/675913/). Important perspectives on AI, academia's role and ethics. It's a particularly interesting read if you work in academia.

<!-- https://taylor.town/chatgpt-opt-out -->
<!-- - [Hedgehog Review: The Dao of Using Your Smartphone](https://hedgehogreview.com/web-features/thr/posts/the-dao-of-using-your-smartphone) -->
