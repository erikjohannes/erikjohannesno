---
title: "Exploring Green AI"
date: 2023-06-10
type: ["posts"]
draft: true
tags:
categories:
---

### Green AI metrics


There are many different ways of 

- **Running time**[^1]. Only suitable for models that are run with the same hardware and software settings.
    - Training time
    - Inference time
    - GPU/CPU hours
- **Carbon emissions**[^1][^3]. This is the most direct measure of actual environmental impact.
    - CO2-equivalents.
    - Location-dependent.
- **Electricity usage**[^3][^4].
    - Energy (Joules, kWh)
    - Power consumption (Watts)
- **Model size** and/or **number of parameters**[^1][^2][^4]. Can be used to calculate the resources needed for training and inference.
- **Floating point operations (FPOs)**[^1][^2][^4]. Almost independent of the software and hardware platforms.


### What can we use these metrics for?

- Quantify emissions
- Choose data centers with care
- Choose more efficient hardware


[^1]: Xu et al. (2021): [A Survey of Green Deep Learning](https://arxiv.org/abs/2111.05193)
[^2]: Fischer et al. (2023): [Energy Efficiency Considerations for Popular AI Benchmarks](https://arxiv.org/abs/2304.08359)
[^3]: Schwartz et al. (2019): [Green AI](https://arxiv.org/abs/1907.10597)
[^4]: Henderson et al. (2020): [Towards the Systematic Reporting of the Energy and Carbon
Footprints of Machine Learning](https://arxiv.org/abs/2002.05651)
