---
title: "Links #5"
date: 2024-12-02T13:27:23+02:00
type: "posts"
draft: false
---

Another collection of interesting reads from around the web:

- [Leaving the phone at home](https://josem.co/leaving-the-phone-at-home/).	This is an interesting exercise. I'm surprised by how different I feel just by leaving that device at home. It's a bit worrying.
- [30 bits of advice for 30 years](https://arne.me/articles/30-bits-of-advice-for-30-years).
- [Solitude and Leadership](https://theamericanscholar.org/solitude-and-leadership/).
- [Stephen Fry Reads Nick Cave’s Stirring Letter About ChatGPT and Human Creativity: “We Are Fighting for the Very Soul of the World”](https://www.openculture.com/2023/11/stephen-fry-reads-nick-caves-stirring-letter-about-chatgpt-and-human-creativity-we-are-fighting-for-the-very-soul-of-the-world.html#google_vignette)
- [Confessions of a Viral AI Writer](https://www.wired.com/story/confessions-viral-ai-writer-chatgpt/). About ChatGPT and the writing process:
> I recalled Zadie Smith’s essay “Fail Better,” in which she tries to arrive at a definition of great literature. She writes that an author’s literary style is about conveying “the only possible expression of a particular human consciousness.” Literary success, then, “depends not only on the refinement of words on a page, but in the refinement of a consciousness.”
- [How it feels to get an AI email from a friend](https://mrgan.com/ai-email-from-a-friend/)
- [Jim Nielsen: It’s Humans All the Way Down](https://blog.jim-nielsen.com/2024/humans-all-the-way-down/).
<!-- - [User:Becha/AI is ecocide](https://wiki.techinc.nl/User:Becha/AI_is_ecocide) -->
<!-- - [notebooks are McDonalds of code](https://yobibyte.github.io/notebooks.html). Not sure if I agree with everything, but I'm not a notebook fan. -->

<!-- You wonder what criteria I use for which links to share? -->
<!-- I only have a simple one: If it's intriguing enough to keep my attention and make me read the whole thing, then it's worth sharing, in my opinion. -->
