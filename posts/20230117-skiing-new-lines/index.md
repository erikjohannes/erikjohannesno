---
date: 2023-01-17T09:00:00
title: Skiing new lines
type: ["posts"]
draft: false
tags:
categories:
---

One of my new year's resolutions: Always ski a new line through the forest when going back to the cabin. It surprised me how much fun I can have in my own backyard just by exploring a bit more.

![This is the year of glade skiing for me.](posts/20230117-skiing-new-lines/1.jpeg)
*This is the year of glade skiing for me.*
