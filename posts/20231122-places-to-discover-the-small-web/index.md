---
title: "Places to discover the Small Web"
date: 2023-11-22T04:47:12+02:00
type: ["posts"]
draft: false
tags:
categories:
---

I'm fascinated by the world of personal websites that exists on the Internet, often referred to as the Small Web.
It's a stark contrast to the "commerical web" and social media platforms, with endless scroll, advertisements and surveillance.
The Small Web let's you browse and discover at your own pace, with fewer distractions and less screaming for attention.
Content can speak for itself, and it's not pushed in your face.

The Small Web doesn't have any built-in mode of discovery, which I think it's one of its strengths.
One of my favourite ways of discovering other people in the Small Web is by recommendations from others, which is why I think it's useful to have a blogroll ([here's mine](bookmarks.html)).
In addition to that, there are several websites that serve as good entry points, and I've tried to compile a list of them here.
If you have any other good suggestions, let me know; I will update this list consecutively.

Here are some places to discover other people's ideas, thoughts and projects:

- [The Forest](https://theforest.link)
- [1MB Club](https://1mb.club/)
- [The 512KB Club](https://512kb.club/)
- [The 250KB Club](https://250kb.club/)
- [10 KB Club](https://10kbclub.com/)
- [1kB Club](https://1kb.club/)
- [blogs.hn](https://blogs.hn/)


Read more about the Small Web here:

- [Ben Hoyt: The small web is beautiful](https://benhoyt.com/writings/the-small-web-is-beautiful/)
- [Aral Balkan: What is the Small Web?](https://ar.al/2020/08/07/what-is-the-small-web/)
