---
title: "Democratizing generative AI"
date: 2023-06-05T19:00
type: ["posts"]
draft: False
tags:
categories:
---

<!-- For et par uker siden skrev en kollega og jeg en kronikk i Teknisk Ukeblad hvor vi leker oss med tanken om hvordan vi kan demokratisere generativ kunstig intelligens (KI), og unngå at vi ender opp med et monopol på store generative KI-modeller. -->
<!-- De mulige scenarioene vi ser oss er ikke alle nødvendigvis realistiske, men vi ønsker å framheve mulighetene vi har for å gjøre bruken og utviklingen av KI mer transparent. -->

<!-- Les kronikken her: [ChatGPT krever moderering à la Wikipedia](https://www.tu.no/artikler/chatgpt-krever-moderering-a-la-wikipedia/530948) (tittelen var opprinnelig **Demokratisering av generativ KI**, som vi synes er litt mer representativ for hva vi ønsket å formidle). -->

A few weeks ago, a colleague and I wrote an op-ed in Teknisk Ukeblad where we played around with the idea of how to democratize generative artificial intelligence (AI) and avoid ending up with a monopoly on large generative AI models. 
The possible scenarios we envision may not all be realistic, but we want to highlight the possibilities we have to make the use and development of AI more transparent. 

The op-ed is in Norwegian, but could be translated to English using various translation tools.
Read the op-ed here: [ChatGPT requires moderation like Wikipedia](https://www.tu.no/artikler/chatgpt-krever-moderering-a-la-wikipedia/530948) (the original title was **Democratizing Generative AI**, which we feel is a bit more representative of what we wanted to convey).
