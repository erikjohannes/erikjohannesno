## Mountain routes

An overview over routes, linkups, traverses, circuits etc in the mountains of Norway.

- [Dovrefjell 2K](mountain-routes_dovrefjell-2k.html)
- [Hurrungane 2K](mountain-routes_hurrungane-2k.html)
- [Rondane 2K](mountain-routes_rondane-2k.html)
- [Romsdalsfjord Skyline](mountain-routes_romsdalsfjord-skyline.html)
- [Trekanten i Trollheimen (Trollheimen Triangle)](mountain-routes_trekanten-i-trollheimen.html)


Coming soon:

- [Høgruta i Jotunheimen (Jotunheimen Haute Route)](mountain-routes_hogruta-i-jotunheimen.html)
- [De syv søstre (The Seven Sisters)](mountain-routes_de-syv-sostre.html)
- [Ersfjord traverse](mountain-routes_ersfjordtraversen.html)
- [Norway 2K](mountain-routes_norway-2k.html)
- [Romsdal 7 Summits](mountain-routes_romsdal-7-summits.html)



### Other resources

Several other websites exist for showing FKT's (Fastest Known Times) if mountain routes:

- [Fastest Know Time](https://fastestknowntime.com/). Worldwide FKT's.
- [mtnath.com: Mountain FKT's](https://mtnath.com/fkt/). Worldwide FKT's, focused on mountain/alpine running and mountaineering.
- [Nå er det alvor: FKT'er i Norge](https://naerdetalvor.no/fkter-i-norge/). Exclusively FKT's in Norway.


Why have I made another collection of FKT's?
Mainly for my own interest, and for collecting certain information is missing from other sources.
I focus mainly on Norwegian mountain routes, and want to include more details and history on each route.
