## Bookmarks

Interesting content around the web, whether it's blogs, articles, podcasts etc.

Topics:

<!-- - [Personal websites](bookmarks.html#personal-websites) -->
<!-- - [Mountain sports](bookmarks.html#mountains-sports) -->
<!-- - [Articles](bookmarks.html#articles) -->
<!-- - [Podcasts](bookmarks.html#podcasts) -->
<!-- - [Humane technology](bookmarks.html#humane-technology) -->

<p style="text-align:center;">
    <a href="bookmarks.html#personal-websites">Personal websites</a>
    | <a href="bookmarks.html#mountains-sports">Mountain sports</a>
    | <a href="bookmarks.html#articles">Articles</a>
    | <a href="bookmarks.html#podcasts">Podcasts</a>
    | <a href="bookmarks.html#humane-technology">Humane technology</a>
</p>

<h3 id="personal-websites">Personal websites</h3>

<!-- Here's a few of the blogs and websites I regularly read: -->

- [Random Notes](https://scottnesbitt.online/)
- [Tim Hårek](https://timharek.no/)
- [47nil](https://47nil.com/)
- [Ploum.net](https://ploum.net/)
- [Jose M. Gilgado](https://josem.co/)
<!-- - [The Sense Common](https://thesensecommon.org) -->
<!-- - [Justin Vollmer](https://www.justinvollmer.com/) -->
<!-- - [Willem's Blog](https://willem.com/blog/) -->
<!-- - [koray er](https://korayer.de/) -->
<!-- - [Garrit Franke](https://garrit.xyz/) -->
<!-- - [Dallin Crump](https://dallincrump.com/) -->
<!-- - [100r](https://100r.co/) -->
<!-- - [The Monday Kickoff](https://mondaykickoff.com/) -->
<!-- - [Thricegreat](https://thricegreat.neocities.org/) -->

<h3 id="mountain-sports">Mountain sports</h3>

- [mtnath.com](https://mtnath.com/) – Personal website and resource hub for mountaineering endurance sports by Kilian Jornet
- [Nå er det alvor](https://naerdetalvor.no/) – Norwegian media house for mountain/ultra/trail running and mountain related activities

<h3 id="articles">Articles</h3>

<!-- This list is inspired by the idea of a [text playlist](https://scottnesbitt.online/on-the-text-playlist). -->
<!-- It's a set of articles or blog posts --> 
Some of my favourite articles I've found on the web:

- [lowering your life's requirements](https://mnmlist.com/requirements/)

<h3 id="Podcasts">Podcasts</h3>
- [Your Undivided Attention](https://www.humanetech.com/podcast) – About new technologies and how they affect humans.
- [Tech Won't Save Us](https://www.techwontsave.us/) – A critical look on technology development.
<!-- - [Practical AI](https://changelog.com/practicalai). -->

<h3 id="human-technology">Humane technology</h3>

- [Low←Tech Magazine](https://solar.lowtechmagazine.com/) – Highlighting old and forgotten technological solutions that may help us live more sustainably.
- [No Tech Magazine](https://www.notechmagazine.com/) – "Technology for Luddites"
- [European alternatives](https://european-alternatives.eu/) – European alternatives for digital services and products (including open-source projects that can be self-hosted), for a less USA-dependent digital world.
