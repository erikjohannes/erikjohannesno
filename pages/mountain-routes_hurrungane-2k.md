## [Mountain routes](mountain-routes.html) / Hurrungane 2K

Linking all summits above 2000 masl of the Hurrungane mountain range in Jotunheimen, Norway.

The route has 23 summits with over 30 meters of prominence.
There is no traditional start and end point.


### Notable performances

- 2014-07-21: Sondre Kvambekk
    - [Report (peakbook.org)](https://peakbook.org/no/tour/94487/Hurrungane+2K-maraton.html)
    - Time from start to last summit: 19:03.
    - 44 km / 6715m
- 2020-07-24: Kilian Jornet
    - [Report (facebook.com)](https://www.facebook.com/kilianjornet/posts/yesterday-i-went-to-hurrungane-for-the-first-time-even-if-it-is-only-a-3h-drive-/10157197720895178/)
    - [Video (youtube.com)](https://www.youtube.com/watch?v=3G29Xr6rBVI)
