## About

*Last updated February 6th, 2025.*

### About me

<!-- I'm Erik Johannes Husom from Norway. I have a passion for being outside in nature, especially in the mountains, and I compete in running (mountain/ultra/sky) and ski mountaineering. -->

I work as a research scientist in the field of Artificial Intelligence (AI), and have a strong interest in sustainable and trustworthy AI.
Take a look at my [Work](work.html)-page for more details on my research.

My main contact point is by email: [erikjohannes@protonmail.com](mailto:erikjohannes@protonmail.com).
I'm also present at the social medium [Mastodon](https://mastodon.social/@erikjohannes).

### About this website

**Analytics and tracking:** I don't use any form of analytics or user tracking on this website. No information about you is collected, and I have no idea how many visits I have.

**Use of generative AI:** I'm wary about the use of such technology in communication between humans, and I think it's important to disclose when it is being used.
As a principle, all content on this website is created by me, without any involvement of AI tools.
Exceptions may occur if I'm using examples of output from AI tools in one of my blog posts, but this will be clearly indicated.

#### How do I make this website?

**For the technical people**: I write the content in either Markdown or HTML, and I have written a small [script](https://codeberg.org/erikjohannes/erikjohannesno/src/branch/pages/buildsite.py) to convert everything to HTML and put the pages together in the correct layout and structure.

**For the non-technical people**: The above paragraph may sound complicated and technical, but making a website can be very easy and is accessible for everyone!
Having your own space on the Internet gives you much more control of the things you share, rather than relying on social media profiles, where you practically give away your content to big tech companies.
If you want to set up your own site and need some help, [let me know](mailto:erikjohannes@protonmail.com)!
