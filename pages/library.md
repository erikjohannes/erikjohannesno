## Library


### Papers


#### Green AI


- [Smaller, faster, greener: Compressing pre-trained code models via surrogate-assisted optimization](https://arxiv.org/abs/2309.04076) *(Shi et al., 2023)*
- [Efficiency is not enough: A critical perspective of environmentally sustainable AI](https://arxiv.org/abs/2309.02065v1) *(Wright et al., 2023)*
- [Clover: Towards sustainable AI with carbon-aware machine learning inference service](https://arxiv.org/abs/2304.09781) *(Li et al., 2023)*
- [How to estimate carbon footprint when training deep learning models? A guide and review](https://arxiv.org/abs/2306.08323) *(Heguerte et al., 2023)*
	- The framework [CodeCarbon](codecarbon.io) is the most accurate tool when compared to a wattmeter, and it adds the least energy overhead when performing the measuring.
- [The cost of understanding – XAI algorithms towards sustainable ML in the view of computational cost](https://www.mdpi.com/2079-3197/11/5/92) *(Jean-Quartier et al., 2023)*
- [Towards energy-efficient deep learning: An overview of energy-efficient approaches along the deep learning lifecycle](https://arxiv.org/pdf/2303.01980) *(Mehlin et al., 2023)*
- [A systematic review of Green AI](https://arxiv.org/abs/2301.11047) *(Verdecchia et al., 2023)*
- [Counting Carbon: A Survey of Factors Influencing the Emissions of Machine Learning](https://arxiv.org/pdf/2302.08476) *(Luccioni et al., 2022)*
	- Survey of the latest decade of machine learning.
	- Carbon footprint of ML is increasing.
- [Estimating the carbon footprint of BLOOM, a 176B parameter language model](https://arxiv.org/abs/2211.02001) *(Luccioni et al., 2022)*
	- Good example of how to measure and report the carbon footprint of an ML model.
- [eco2AI: Carbon emissions tracking of machine learning models as the first step towards sustainable AI](https://arxiv.org/abs/2208.00406) *(Budennyy et al., 2022)*
- [The carbon footprint of machine learning training will plateau, then shrink](https://arxiv.org/abs/2204.05149) *(Patterson et al., 2022)*
- [Green AI: Do deep learning frameworks have different costs?](https://stefanos1316.github.io/my_curriculum_vitae/GKSSZ22.pdf) *(Georgiou et al., 2022)*
	- Tensorflow achieves significantly better energy and run-time performance than Pytorch, and withe large effect sizes in 100% of the cases for the training phase.
	- Pytorch exhibits significantly better energy and run-time performance than Tensorflow in the inference phase for 66% of the cases, always, with large effect sizes.
	- The difference in performance costs does not seem to affect the accuracy of the models produced, as both frameworks achieve comparable scores under the same configurations.
- [A survey on green deep learning](https://arxiv.org/abs/2111.05193) *(Xu et al., 2021)*
- [Energy and policy considerations for deep learning in NLP](https://arxiv.org/abs/1906.02243) *(Strubell et al., 2019)*
- [Green AI](https://arxiv.org/abs/1907.10597) *(Schwartz et al., 2019)*
	- Establishing the concepts of "Green AI" and "Red AI".
