## AI and sustainability


I collect noteworthy resources about the topic of AI and environmental sustainability. 

Quicklinks:

<p style="text-align:center;">
    <a href="ai-and-sustainability.html#news">News</a>
    | <a href="ai-and-sustainability.html#papers">Papers</a>
    | <a href="ai-and-sustainability.html#reports">Reports</a>
    | <a href="ai-and-sustainability.html#norwegian">Norwegian media</a>
    | <a href="ai-and-sustainability.html#tools">Tools</a>
    | <a href="ai-and-sustainability.html#leaderboards">Leaderboards</a>
    | <a href="ai-and-sustainability.html#resources">Other resources</a>
</p>

<h3 id="news">News</h3>

- 08 Feb 2025: [Ultra-efficient AI won’t solve data centers’ climate problem. This might.](https://www.washingtonpost.com/climate-solutions/2025/02/07/deepseek-ai-efficiency-climate-change/). Summary: Adaptively use less resource-demanding inference processes when the carbon intensity of the power grid is high.
- 15 Jan 2025: [Enterprises in for a shock when they realize power and cooling demands of AI](https://www.theregister.com/2025/01/15/ai_power_cooling_demands/)
- 13 Jan 2025: [Water shortage fears as Labour’s first AI growth zone sited close to new reservoir](https://www.theguardian.com/technology/2025/jan/13/labour-ai-datacentre-growth-zone-water-shortages-abingdon-reservoir)
- 09 Jan 2025: [To save the energy grid from AI, use open source AI software](https://www.theregister.com/2025/01/09/linux_foundation_ai_energy_report/) 

<h3 id="papers">Papers</h3>

- From Efficiency Gains to Rebound Effects: The Problem of Jevons’ Paradox in AI’s Polarized Environmental Debate (Luccioni et al. 2025) [[Paper]](https://arxiv.org/pdf/2501.16548v1)
- SPROUT: Green Generative AI with Carbon-Efficient LLM Inference (Li et al. 2024) [[Paper]](https://aclanthology.org/2024.emnlp-main.1215.pdf).
- Artificial Intelligence in Climate Change Mitigation: A Review of Predictive Modeling and Data-Driven Solutions for Reducing Greenhouse Gas Emissions (Adegbite et al. 2024) [[Paper]](https://wjarr.com/sites/default/files/WJARR-2024-3043.pdf)
- Addition is All You Need for Energy-Efficient Language Models (Luo et al. 2024) [[Paper]](https://arxiv.org/pdf/2410.00907)
- LLMCO2: Advancing Accurate Carbon Footprint Prediction for LLM Inferences (Fu et al. 2024) [[Paper]](https://arxiv.org/abs/2410.02950)
- AI, Climate, and Regulation: From Data Centers to the AI Act (Erbert et al. 2024) [[Paper]](https://arxiv.org/pdf/2410.06681)
- Offline Energy-Optimal LLM Serving: Workload-Based Energy Models for LLM Inference on Heterogeneous Systems (Wilkins et al. 2024) [[Paper]](https://arxiv.org/pdf/2407.04014)
- Hybrid Heterogeneous Clusters Can Lower the Energy Consumption of LLM Inference Workloads (Wilkins et al. 2024) [[Paper]](https://dl.acm.org/doi/pdf/10.1145/3632775.3662830)
- The Price of Prompting: Profiling Energy Use in Large Language Models Inference (Husom et al. 2024) [[Paper]](https://arxiv.org/abs/2407.16893)
- Towards Greener LLMs: Bringing Energy-Efficiency to the Forefront of LLM Inference (Stojkovic et al. 2024) [[Paper]](https://arxiv.org/abs/2403.20306)
- Towards Efficient Generative Large Language Model Serving: A Survey from Algorithms to Systems (Miao et al. 2024) [[Paper]](https://arxiv.org/abs/2312.15234)
- Beyond Efficiency: Scaling AI Sustainably (Wu et al. 2024) [[Paper]](https://arxiv.org/abs/2406.05303v1)
- A Simplified Machine Learning Product Carbon Footprint Evaluation Tool (Lang et al. 2024) [[Paper]](https://www.sciencedirect.com/science/article/pii/S2666789424000254)
- Green AI: Exploring Carbon Footprints, Mitigation Strategies, and Trade Offs in Large Language Model Training (Liu et al. 2024) [[Paper]](https://arxiv.org/abs/2404.01157)
- Measuring and Improving the Energy Efficiency of Large Language Models Inference (Argerich et al. 2024) [[Paper]](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=10549890) [[GitHub]](https://github.com/maufadel/EnergyMeter)
- Engineering Carbon Emissions Aware Machine Learning Pipelines (Husom et al. 2024) [[Paper]](https://dl.acm.org/doi/pdf/10.1145/3644815.3644943)
- Timeshifting Strategies for Carbon-Efficient Long-Running Large Language Model Training (Jagannadharao et al. 2024) [[Paper]](https://link.springer.com/article/10.1007/s11334-023-00546-x)
- Perseus: Reducing Energy Bloat in Large Model Training (Chung et al. 2024) [[Paper]](https://arxiv.org/abs/2312.06902)
-  Counting Carbon: A Survey of Factors Influencing the Emissions of Machine Learning (Luccioni et al. 2023) [[Paper]](https://arxiv.org/pdf/2302.08476v1.pdf)
-  A Systematic Review of Green AI (Verdecchia et al. 2023) [[Paper]](https://arxiv.org/pdf/2301.11047.pdf)
- Power Hungry Processing: Watts Driving the Cost of AI Deployment? (Luccioni et al. 2023) [[Paper]](https://arxiv.org/pdf/2311.16863.pdf)
- LLMCarbon: Modeling the End-To-End Carbon Footprint of Large Language Models (Faiz et al. 2023) [[Paper]](https://arxiv.org/pdf/2309.14393.pdf)
- Chasing Low-Carbon Electricity for Practical and Sustainable DNN Training (Yang et al. 2023) [[Paper]](https://www.climatechange.ai/papers/iclr2023/29)
- Zeus: Understanding and Optimizing GPU Energy Consumption of DNN Training (You et al. 2023) [[Paper]](https://www.usenix.org/conference/nsdi23/presentation/you)
- Making AI Less "Thirsty": Uncovering and Addressing the Secret Water Footprint of AI Models (Li et al. 2023) [[Paper]](https://arxiv.org/pdf/2304.03271.pdf)
- Method and Evaluations of the Effective Gain of Artificial Intelligence Models for Reducing CO2 Emissions (Delanoë et al. 2023) [[Paper]](https://www.sciencedirect.com/science/article/pii/S030147972300049X)
- **Measuring the Environmental Impacts of Artificial Intelligence Compute and Applications** (OECD 2022) [[Paper]](https://www.oecd-ilibrary.org/docserver/7babf571-en.pdf?expires=1701262318&id=id&accname=guest&checksum=FAB39144A63BB5953FF7D56D7C18B147)
- Towards Sustainable Artificial Intelligence: An Overview of Environmental Protection Uses and Issues (Pachot et al. 2022) [[Paper]](https://arxiv.org/ftp/arxiv/papers/2212/2212.11738.pdf)
- Towards the Systematic Reporting of the Energy and Carbon Footprints of Machine Learning (Henderson et al. 2022) [[Paper]](https://arxiv.org/pdf/2002.05651.pdf)
- The Carbon Footprint of Machine Learning Training Will Plateau, Then Shrink (Patterson et al. 2022) [[Paper]](https://arxiv.org/ftp/arxiv/papers/2204/2204.05149.pdf)
- A First Look into the Carbon Footprint of Federated Learning (Qiu et al. 2022) [[Paper]](https://arxiv.org/pdf/2102.07627.pdf)
- Sustainable AI: Environmental Implications, Challenges and Opportunities (Wu et al. 2022) [[Paper]](https://arxiv.org/pdf/2111.00364.pdf)
- Environmental Assessment of Projects Involving AI Methods (Lefèvre et al. 2022) [[Paper]](https://hal.science/hal-03922093v1/document)
- Eco2AI: Carbon Emissions Tracking of Machine Learning Models (Budennyy et al. 2022) [[Paper]](https://arxiv.org/pdf/2208.00406.pdf)
- Bridging Fairness and Environmental Sustainability in Natural Language Processing (Hessenthaler et al. 2022) [[Paper]](https://arxiv.org/pdf/2211.04256.pdf)
- Estimating the Carbon Footprint of BLOOM: A 176B Parameter Language Model (Luccioni et al. 2022) [[Paper]](https://arxiv.org/pdf/2211.02001.pdf)
- Measuring the Carbon Intensity of AI in Cloud Instances (Dodge et al. 2022) [[Paper]](https://arxiv.org/pdf/2206.05229.pdf)
- Unraveling the Hidden Environmental Impacts of AI Solutions for Life Cycle Assessment (Ligozat et al. 2022) [[Paper]](https://arxiv.org/pdf/2110.11822.pdf)
-  A Survey on Green Deep Learning (Xu et al. 2021) [[Paper]](https://arxiv.org/pdf/2111.05193.pdf)
-  Evaluating the Carbon Footprint of NLP Methods: A Survey and Analysis of Existing Tools (Bannour et al. 2021) [[Paper]](https://aclanthology.org/2021.sustainlp-1.2.pdf)
- New Universal Sustainability Metrics to Assess Edge Intelligence (Lenherr et al. 2021) [[Paper]](https://www.sciencedirect.com/science/article/pii/S2210537921000718?via%3Dihub)
- **Aligning Artificial Intelligence with Climate Change Mitigation** (Kaack et al. 2021) [[Paper]](https://hal.archives-ouvertes.fr/hal-03368037/document)
- A Framework for Energy and Carbon Footprint Analysis of Distributed and Federated Edge Learning (Savazzi et al. 2021) [[Paper]](https://arxiv.org/pdf/2103.10346.pdf)
- A Practical Guide to Quantifying Carbon Emissions for Machine Learning Researchers and Practitioners (Ligozat et al. 2021) [[Paper]](https://hal.archives-ouvertes.fr/hal-03376391/document)
- Green Algorithms: Quantifying the Carbon Footprint of Computation (Lannelongue et al. 2021) [[Paper]](https://onlinelibrary.wiley.com/doi/10.1002/advs.202100707)
- Carbon Emissions and Large Neural Network Training (Patterson et al. 2021) [[Paper]](https://arxiv.org/ftp/arxiv/papers/2104/2104.10350.pdf)
- Chasing Carbon: The Elusive Environmental Footprint of Computing (Gupta et al. 2020) [[Paper]](https://arxiv.org/pdf/2011.02839.pdf)
- Carbontracker: Tracking and Predicting the Carbon Footprint of Training Deep Learning Models (Anthony et al. 2020) [[Paper]](https://arxiv.org/pdf/2007.03051.pdf)
- **Green AI** (Schwartz et al. 2020) [[Paper]](https://cacm.acm.org/magazines/2020/12/248800-green-ai/fulltext)
- Quantifying the Carbon Emissions of Machine Learning (Lacoste et al. 2019) [[Paper]](https://arxiv.org/pdf/1910.09700.pdf)
- **Energy and Policy Considerations for Deep Learning in NLP** (Strubell et al. 2019) [[Paper]](https://arxiv.org/pdf/1906.02243.pdf)

<h3 id="reports">Reports</h3>

- [The Environmental Impacts of AI - Policy Primer](https://www.sashaluccioni.com/assets/AI%20+%20Environment%20Primer%20(Hugging%20Face).pdf) (Hugging Face).

<h3 id="norwegian">Norwegian media (<i>Norsk media</i>)</h3>

- 26 des 2024: [Spørsmål til ChatGPT sluker vatn: – Burde vere ei øvre grense for tøys og tull](https://www.nrk.no/norge/chatgpt-sluker-store-mengder-vatn-1.17068546)
- 16 okt 2024: [Atomkraft + KI = sant?](https://www.nrk.no/arkiv/artikkel/atomkraft-_-ki-_-sant_-1.17076855)
- 02 okt 2024: [KI forurenser 10 ganger mer enn Google – slik bør du bruke det](https://www.nrk.no/nordland/chatgpt-forurenser-10-ganger-mer-enn-google_-_-selvfolgelig-altfor-hoyt-1.17054261)



<h3 id="tools">Tools</h3>

#### A. Tools for Measuring and Quantifying Footprint

- AIPowerMeter [[Website]](https://greenai-uppa.github.io/AIPowerMeter/) [[Source code]](https://github.com/GreenAI-Uppa/AIPowerMeter)
- CarbonAI [[Source code]](https://github.com/Capgemini-Invent-France/CarbonAI)
- carbontracker [[Source code]](https://github.com/lfwa/carbontracker) [[Paper]](https://arxiv.org/pdf/2007.03051.pdf)
- CodeCarbon [[Website]](https://codecarbon.io/) [[Source code]](https://github.com/mlco2/codecarbon) [[Paper]](https://arxiv.org/pdf/1911.08354.pdf)
- d2m [[Website]](https://sintef-9012.github.io/d2m/) [[Source code]](https://github.com/SINTEF-9012/d2m)
- Eco2AI [[Source code]](https://github.com/sb-ai-lab/Eco2AI) [[Paper]](https://arxiv.org/pdf/2208.00406.pdf)
- EcoLogits [[Source code]](https://github.com/genai-impact/ecologits) [[Documentation]](https://ecologits.ai/latest/)
- EnergyMeter [[Source code]](https://github.com/maufadel/EnergyMeter) [[Paper]](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=10549890)
- experiment-impact-tracker [[Source code]](https://github.com/Breakend/experiment-impact-tracker) [[Paper]](https://arxiv.org/pdf/2002.05651.pdf)
- powermeter [[Source code]](https://github.com/autoai-incubator/powermeter)
- pyJoules [[Source code]](https://github.com/powerapi-ng/pyJoules)
- tracarbon [[Source code]](https://github.com/fvaleye/tracarbon)
- zeus [[Website]](https://ml.energy/zeus) [[Source code]](https://github.com/ml-energy/zeus) [[Paper]](https://www.usenix.org/system/files/nsdi23-you.pdf)

#### B. Tools for Calculation/Estimation of Footprint
- Green Algorithms [[Website]](http://calculator.green-algorithms.org/) [[Paper]](https://onlinelibrary.wiley.com/doi/epdf/10.1002/advs.202100707)
- ML CO2 Impact [[Website]](https://mlco2.github.io/impact/) [[Paper]](https://arxiv.org/pdf/1910.09700.pdf)


<h3 id="leaderboards">Leaderboards</h3>

- [LEADERBOARD]: [LLM-Perf Leaderboard](https://huggingface.co/spaces/optimum/llm-perf-leaderboard)
- [LEADERBOARD]: [ML.ENERGY Leaderboard](https://ml.energy/leaderboard/)

<h3 id="resources">Other resources</h3>

- Green Software Foundation – Promoting Sustainable Software Development [[Website]](https://greensoftware.foundation/)
- ENFIELD – European Lighthouse to Manifest Trustworthy and Green AI, a Centre of Excellence for Green AI [[Website]](https://www.enfield-project.eu/)
- [Awesome Green AI](https://github.com/samuelrince/awesome-green-ai/tree/main) by [samuelrince](https://github.com/samuelrince)
