## More

- [Bookmarks](bookmarks.html)
- [AI and sustainability](ai-and-sustainability.html)
- [Mountain routes](mountain-routes.html)
- [Software projects](projects.html)
