## [Mountain routes](mountain-routes.html) / Romsdalsfjord Skyline

Linking the main ridges around Romsdalsfjord in Rauma, Norway.

- Kilian Jornet
    - [Report (mtnath.com)](https://mtnath.com/romsdalsfjord-skyline/)
    - [Map (wikiloc.com)](https://www.wikiloc.com/running-trails/romsdalfjord-round-30684765)


