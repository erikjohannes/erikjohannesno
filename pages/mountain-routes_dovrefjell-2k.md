## [Mountain routes](mountain-routes.html) / Dovrefjell 2K

Linking all summits above 2000 masl of the Dovrefjell mountain range in Norway.

### Key info

- Distance: 34 km
- Elevation gain: 3400 m
- Start/end: Snøheim
- Summits:
    - Bruri (2001 masl) 
    - Hettpiggen (2261 masl)
    - Store Langvasstinden (2085 masl)
    - Langvasstinden V2 (2025 masl) 
    - Vestre Langvasstinden (2046 masl) 
    - Larstinden (2108 masl)
    - Nørdre Larstinden (2077 masl) 
    - Store Skredahøe (2004 masl)
    - Snøhetta (2286 masl)
    - Snøhetta Midttoppen (2278 masl)
    - Snøhetta Vesttoppen (2253 masl)
    - Storstygge-Svånåtinden (2209 masl)
    - Nørdre Svånåtinden (2004 masl)

### Description

The route starts and ends at the lodge Snøheim.
Most of the route is off-trail and goes through very rocky terrain, and to complete the circuit without significant detours one has to climb or abseil several sections.

### Notable performances

- 2016-08-23: Sondre Kvambekk - 08:53
    - [Report (peakbook.org)](https://peakbook.org/no/tour/180131/Dovrefjell+2K-maraton.html)
