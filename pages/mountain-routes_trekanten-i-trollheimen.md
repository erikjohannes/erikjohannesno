## [Mountain routes](mountain-routes.html) / Trekanten i Trollheimen (Trollheimen Triangle)

The Trollheimen Triangle is a classic hiking route between the three lodges Gjevilvasshytta, Jøldalshytta, and Trollheimshytta, forming a complete triangle by going back to Gjevilvasshytta at the end.


### Key info

- Distance: 60 km
- Elevation gain: 2000 m
- Start/end: Gjevilvasshytta
- Checkpoints:
    - Jøldalshytta
    - Trollheimshytta

### Description

There are several variations on this route, but the fastest one does not touch any summits, but goes through the valleys/passes between each lodge.


### Notable performances


- 2013-08-17: Ola Hovdenak - 05:59:43
    - [Article (adressa.no)](https://www.adressa.no/nyheter/trondelag/i/bzP7X5/fem-timer-og-femtini-minutter)
- 2019-08-03: Molly Bazilchuck - 07:45:02 - **current FKT (female)**
    - [Report (strava.com)](https://www.strava.com/activities/2586959904)
    - <details>
    <summary>[FKT link and report (fastestknowtime.com)](https://fastestknowntime.com/fkt/molly-bazilchuk-trollheimen-triangle-norway-2019-08-03)</summary>
    <blockquote>Amazing day, beautiful weather and Trollheimen at its most beautiful! I had planned to do the Triangle as a long training day, but it was one of those days where everything clicked. I started at Gjevilvasshytta, and the easy, runnable first 20 km to Jøldalshytta just flew by. I eased off the gas a bit going over Geithetta, being careful on the rocky sections. At Trollheimshytta, I saw that my husband, who was riding the same loop on a mountain bike, had checked into the hut just 15 min before me, which gave me motivation to push up the next big climb and pass him. During the final 15 km I started to realize a sub-8 hour finish was possible, and pushed a bit. On the final descent I unfortunately fell and scraped my knee, which slowed me down a bit.  No resupply at the huts, just carried all my calories and refilled water in streams along the way.
</blockquote>
    </details>
    - [GPX](gpx/mountain-route_trekanten-i-trollheimen-bazilchuck.gpx)
- 2020-10-??: Sverre Solligård - 05:47:17
    - <details>
    <summary>[Report (facebook.com)](https://www.facebook.com/svorkmonoifriidrett/posts/2796662983925461)</summary>
    <blockquote>Startet fra Gjevillvasshytta ca. kl. 08.30. Oppholdsvær, null vind og 7-8 grader. Tidvis litt vått, men bra forhold totalt sett. Fant en grei flyt, men kjente tidlig at halvmaraton fra helga satt i beina. Lå godt foran skjema på Jøldalshytta, og gikk lett i starten av Svartådalen. Begynte å kjenne gradvis tyngre bein etter ca. 30 km og ned til Trollheimshytta på 37 km, men hadde fortsatt greit håp om rekord der. Brukte 3-4 min på å fylle på drikkeflaska +  påfyll av litt ekstra næring før det bars til opp det verste partiet. Fikk egentlig en skikkelig smell ganske umiddelbart i stigninga opp mot Mellomfjell. Mye gåing me hendene på låra der. Etter ca. 40 km var det krampetendenser i begge leggene, men det ga seg heldigvis etter 5-10 min. På det tidspunktet ga jeg egentlig opp rekorden og håpa å komme meg noenlunde greit igjennom. Det løsna litt når jeg kom opp på Mellomfjell og det slaket ut. Når det var igjen ca. 10 km så jeg at det var håp for rekord likevel, og når det gjennsto 7 km skjønte jeg at det kunne gå med god margin hvis krampene holdt seg unna. Siste 5 km var egentlig en fryd å løpe, når jeg så at det kunne gå. Ble fort litt smånervøs igjen når det var igjen 2,5 km, for da kom det småkramper i leggene og i det ene låret. Heldigvis slapp det etter 500 m. Alt i alt artig å ta rekorden, men frister ikke til gjentagelse! (Hvertfall ikke med det første😉)</blockquote>
    </details>
    <!-- - [GPX](gpx/mountain-route_trekanten-i-trollheimen-solligard.gpx) -->
- 2021-08-17: Steinar Aarvåg - 06:49:43
    - <details>
    <summary>[Report (strava.com)](https://www.strava.com/activities/5806686246)</summary>
    <blockquote>Grått og vått, men bortimot optimal temperatur. Store deler med sti og bekk i ett og samme produkt. Banka til opp fra Trollheimshytta og fikk igjen med renter opp Riarskaret</blockquote>
    </details>
    - [FKT link (fastestknowtime.com)](https://fastestknowntime.com/fkt/steinar-aarvag-trollheimen-triangle-norway-2021-08-17)
    - [GPX](gpx/mountain-route_trekanten-i-trollheimen-aarvag.gpx)
