## Work

I'm a research scientist at [SINTEF Digital](https://www.sintef.no/en/digital/). 
My research interests lie within responsible and ethical use of Artificial Intelligence (AI), including trustworthy AI, explainable AI and green AI.
I work a lot with AI engineering and applied machine learning.

I'm a part of the [Trustworthy Green IoT Software](https://www.sintef.no/en/expertise/digital/sustainable-communication-technologies/trustworthy-green-iot-software/) research group in the [Department of Sustainable Communication Technologies](https://www.sintef.no/en/digital/departments-new/sustainable-communication-technologies/).

For professional inquiries, I can be contacted at [erik.johannes.husom@sintef.no](mailto:erik.johannes.husom@sintef.no).

Active research areas and projects:

- **Green AI**: How can we make AI more energy-efficient and decrease the carbon footprint of generative AI? Project: [ENFIELD](https://www.enfield-project.eu/).
- **Agentic AI**: How can we construct reliable and trustworthy systems of AI agents? Project: [INTEND](https://intendproject.eu/).

<!-- ### Table of contents -->

<!-- - [Software projects](work.html#software-projects) -->
<!-- - [Media](work.html#media) -->
<!-- - [Op-eds (*kronikker*)](work.html#op-eds) -->
<!-- - [Talks and presentations](work.html#presentations) -->
<!-- - [Scientific publications](work.html#scientific-publications) -->

Quicklinks:

<p style="text-align:center;">
    <a href="work.html#media">Media</a>
    | <a href="work.html#op-eds">Op-eds (<i>kronikker</i>)</a>
    | <a href="work.html#software-projects">Software projects</a>
    | <a href="work.html#presentations">Talks/presentations</a>
    | <a href="work.html#scientific-publications">Publications</a>
</p>


<h3 id="media">Media</h3>

- **[Abels Tårn: Forskningsfronten (NRK)](https://radio.nrk.no/serie/abels-taarn-radio/MDFP05001225?utm_source=nrkradio&utm_medium=delelenke-ios&utm_content=prf:MDFP05001225#t=29m4s)**. Intervju hos det populærvitenskapelige radioprogrammet Abels Tårn hos NRK, 6. feb 2025.
- **[What Does OpenAI’s New Breakthrough Mean for Energy Consumption?](https://heatmap.news/technology/openai-o1-energy)**. Interview with Heatmap News, Sep 17th, 2024.

<h3 id="op-eds">Op-eds (<i>kronikker</i>)</h3>

-   **[Klimakostnaden for bruk av Chat GPT kan bli større enn nytteverdien](https://www.digi.no/artikler/debatt-klimakostnaden-for-bruk-av-chat-gpt-kan-bli-storre-enn-nytteverdien/552087?utm_source=digifront&utm_medium=50067&utm_content=row5_pos0)**. Erik Johannes Husom, Finn Lützow-Holm Myrstad (fagdirektør i Forbrukerrådet), Gencer Erdogan (fungerende forskningsleder i Sintef Digital) og Elin Volder Rutle (leder for bærekraft i Forbrukerrådet). digi.no, 28. okt, 2024
-   **[ChatGPT krever moderering á la Wikipedia](https://www.tu.no/artikler/chatgpt-krever-moderering-a-la-wikipedia/530948)**.  Rustem Dautov, Erik Johannes Husom. Teknisk Ukeblad, 16. mai, 2023
-   **[Menneskapt eller ikke – spiller det noen rolle?](https://www.vl.no/meninger/verdidebatt/2023/02/18/menneskeskapt-eller-ikke-spiller-det-noen-rolle/)** Erik Johannes Husom. Vårt Land, 18. februar, 2023
-   **[Er vi klare for metaverset?](https://klassekampen.no/utgave/2022-10-31/debatt-er-vi-klare-for-metaverset)** Erik Johannes Husom, Ketil Stølen. Klassekampen, 31. oktober, 2022

<h3 id="software-projects">Software projects</h3>

- **MELODI** ([Website](https://github.com/ejhusom/MELODI)) ([Source code](https://github.com/ejhusom/MELODI)). Monitoring energy consumption of LLM inference.
- **d2m** ([Website](https://ejhusom.github.io/d2m/)) ([Source code](https://github.com/ejhusom/d2m)). A machine learning pipeline for time series data, enabling responsible use of artificial intelligence.
- **UDAVA** ([Website](https://ejhusom.github.io/Udava/)) ([Source code](https://github.com/ejhusom/Udava/)). Unsupervised data validation.
- **Green AI** ([Website](https://ejhusom.github.io/green-ai/)) ([Source code](https://github.com/ejhusom/green-ai/)). A curated overview of Green AI resources.



<h3 id="presentations">Talks and presentations</h3>

- [KI og bærekraft i et klimaperspektiv](slides/talk-2024-12-02-ki-og-baerekraft-i-et-klimaperspektiv.pdf), innlegg hos NIBIO, 2. desember 2024.
- [Hvordan få til bærekraftig bruk av KI?](slides/talk-2024-11-14-dnd-ki-baerekraftig.pdf), foredrag på halvdagskonferasen "Bærekraftig bruk av AI" av Den norske dataforening, 14. nov 2024.
- [KI og ressursforbruk fra et forbrukerperspektiv](slides/talk-2024-09-03-ki-ressursforbruk-forbrukerradet.pdf), innlegg hos Forbrukerrådet, 03. september 2024.
- [Grønn KI](slides/talk-2024-08-12-arendalsuka-green-ai.pdf), på arrangementet [KI som klimaversting eller reddende engel?](https://program.arendalsuka.no/event/user-view/23542) under Arendalsuka, 12. august 2024.
- [Kunstig intelligens – bare et gode? Etiske utfordringer i møte med ny teknologi](slides/talk-2024-05-30-kunstig-intelligens-og-etikk.pdf), 30. mai 2024.
-   [Engineering Carbon Emissions Aware Machine Learning Pipelines](slides/talk-2024-04-15-engineering-carbon-emission-aware-ml-pipelines.pdf), presentation of conference paper, CAIN 2024, April 15th, 2024
-   [Measuring and understanding energy use in Large Language Model inference](slides/talk-2024-03-21-llm-energy-consumption-fagdrypp.pdf), 21. mars 2024.
-   [Kunstig intelligens og etikk](slides/talk-2024-03-06-kunstig-intelligens-og-etikk.pdf), 6. mars 2024.
-   [Kunstig intelligens – pålitelighet og bærekraft](slides/talk-2024-01-30-green-and-trustworthy-ai.pdf), 30. januar 2024.
-   [Maskinlæringens klimaavtrykk: Hvordan måles det og hva kan vi bruke målingene til?](slides/talk-2023-09-green-ai-gemini-seminar.pdf), foredrag på seminaret ["Bærekraft og maskinlæring – Lar det seg forene?"](https://www.sintef.no/arrangementer-og-kurs/arkiv/2023/barekraft-og-maskinlaring-lar-det-seg-forene/), 21.  september 2023
-   [AutoConf: Automated Configuration of Unsupervised Learning Systems using Metamorphic Testing and Bayesian Optimization](slides/talk-2023-09-ase-autoconf.pdf), presentation of conference paper, ASE 2023, September 14th, 2023
-   [The future of software engineering in the light of LLMs](slides/talk-2023-05-the-future-of-software-engineering.pdf), May 2023
-   [Replay-Driven Continual Learning for the Industrial Internet of Things](slides/talk-2023-05-cain-continual-learning.pdf), presentation of conference article, CAIN 2023, 15th May, 2023
-   [Machine Learning for Fatigue Detection using Fitbit Fitness Trackers](slides/talk-2022-10-icsports-machine-learning-for-fatigue-detection.pdf) , presentation of conference article, icSports 2023, 27th October, 2022
-   [UDAVA: An Unsupervised Learning Pipeline for Sensor Data Validation in Manufacturing](slides/talk-2022-05-cain-unsupervised-learning-pipeline.pdf), presentation of conference article, CAIN 2022, 17th May, 2022

<h3 id="scientific-publications">Scientific publications</h3>

- 19\. **[Raft Protocol for Fault Tolerance and Self-Recovery in Federated Learning](https://ieeexplore.ieee.org/document/10556381)**.
	- Rustem Dautov, **Erik Johannes Husom**.
	- 19th International Conference on Software Engineering for Adaptive and Self-Managing Systems (SEAMS). 2024-04-16.
- 18\. **[Engineering Carbon Emissions Aware Machine Learning Pipelines](https://dl.acm.org/doi/pdf/10.1145/3644815.3644943)**.
	- **Erik Johannes Husom**, Sagar Sen, Arda Goknil.
	- 2024 IEEE/ACM 3rd International Conference on AI Engineering–Software Engineering for AI (CAIN). 2024-04-15.
	- [Link to presentation slides](slides/talk-2024-04-15-engineering-carbon-emission-aware-ml-pipelines.pdf).
- 17\. **[Automated Behavior Labeling for IIoT Data](https://dl.acm.org/doi/abs/10.1145/3627050.3630725)**.
	- **Erik Johannes Husom**, Arda Goknil, Simeon Tverdal, Sagar Sen, Phu Nguyen.
	- The 13th International Conference on the Internet of Things (IoT 2023). 2023-11-07.
- 16\. **[REPTILE: a Tool for Replay-driven Continual Learning in IIoT](https://dl.acm.org/doi/abs/10.1145/3627050.3630739)**.
	- **Erik Johannes Husom**, Sagar Sen, Arda Goknil, Simeon Tverdal, Phu Nguyen.
	- The 13th International Conference on the Internet of Things (IoT 2023). 2023-11-07.
- 15\. **[Towards Community-Driven Generative AI](https://annals-csis.org/proceedings/2023/pliks/position.pdf#page=52)**.
	- Rustem Dautov, **Erik Johannes Husom**, Sagar Sen, Hui Song.
	- Position Papers of the 18th Conference on Computer Science and Intelligence Systems (fedCSIS 2023). 2023-09-17.
- 14\. **[AutoConf: Automated Configuration of Unsupervised Learning Systems Using Metamorphic Testing and Bayesian Optimization](https://ieeexplore.ieee.org/abstract/document/10298518)**.
	- Lwin Khin Shar, Arda Goknil, **Erik Johannes Husom**, Sagar Sen, Yan Naing Tun, Kisub Kim
	- 2023 38th IEEE/ACM International Conference on Automated Software Engineering (ASE 2023). 2023-09-14.
- 13\. **[Uncertainty-aware Virtual Sensors for Cyber-Physical Systems](https://ieeexplore.ieee.org/abstract/document/10225620/)**.
	- Sagar Sen, **Erik Johannes Husom**, Arda Goknil, Simeon Tverdal, Phu Nguyen. 
	- IEEE Software. 2023-08-21. Journal paper.
- 12\. **[Replay-driven continual learning for the industrial internet of things](https://ieeexplore.ieee.org/abstract/document/10164751/)**. 
	- Sagar Sen, Simon Myklebust Nielsen, **Erik Johannes Husom**, Arda Goknil, Simeon Tverdal, Leonardo Sastoque Pinilla. 
	- 2023 IEEE/ACM 2nd International Conference on AI Engineering--Software Engineering for AI (CAIN). 2023-05-15.
	- [Link to presentation slides](slides/talk-2023-05-cain-continual-learning.pdf).
- 11\. **[Virtual sensors for erroneous data repair in manufacturing a machine learning pipeline](https://www.sciencedirect.com/science/article/pii/S0166361523000672)**. 
	- Sagar Sen, **Erik Johannes Husom**, Arda Goknil, Dimitra Politaki, Simeon Tverdal, Phu Nguyen, Nicolas Jourdan. 
	- Computers in Industry, Volume 149, August 2023, 103917. 2023-08-01.  Journal paper.
- 10\. **[A blockchain-based framework for trusted quality data sharing towards zero-defect manufacturing](https://www.sciencedirect.com/science/article/pii/S0166361523000039)**. 
	- Mauro Isaja, Phu Nguyen, Arda Goknil, Sagar Sen, **Erik Johannes Husom**, Simeon Tverdal, Abhilash Anand, Yunman Jiang, Karl John Pedersen, Per Myrseth, Jørgen Stang, Harris Niavis, Simon Pfeifhofer, Patrick Lamplmair. 
	- Computers in Industry, Volume 146, April 2023, 103853. 2023-04-01.  Journal paper.
- 9\. **[Bridging the Gap Between Java and Python in Mobile Software Development to Enable MLOps](https://ieeexplore.ieee.org/abstract/document/9941679/)**.
	- Rustem Dautov, **Erik Johannes Husom**, Fotis Gonidis, Spyridon Papatzelos, Nikolaos Malamas. 
	- 2022 18th International Conference on Wireless and Mobile Computing, Networking and Communications (WiMob). 2022-11-15.
- 8\. **[Machine Learning for Fatigue Detection using Fitbit Fitness Trackers](https://www.scitepress.org/PublicationsDetail.aspx?ID=FrHMX3/m/wY=&t=1)**. 
	- **Erik Johannes Husom**, Rustem Dautov, Adela-Aniela Nedisan, Fotis Gonidis, Spyridon Papatzelos, Nikolaos Malamas. 
	- icSports 2022. 2022-10-28. 
	- [Link to presentation slides](slides/talk-2022-10-icsports-machine-learning-for-fatigue-detection.pdf).
- 7\. **[Taming Data Quality in AI-Enabled Industrial Internet of Things](https://ieeexplore.ieee.org/abstract/document/9845709)**. 
	- Sagar Sen, **Erik Johannes Husom**, Arda Goknil, Simeon Tverdal, Phu Nguyen, Iker Mancisidor. 
	- IEEE Software. 2022-08-01. Journal paper.
- 6\. **[Towards MLOps in Mobile Development with a Plug-in Architecture for Data Analytics](https://ieeexplore.ieee.org/abstract/document/9982701)**. 
	- Rustem Dautov, **Erik Johannes Husom**, Fotis Gonidis. 
	- 2022 6th International Conference on Computer, Software and Modeling (ICCSM). 2022-07-01.
- 5\. **[UDAVA: An Unsupervised Learning Pipeline for Sensor Data Validation in Manufacturing](https://ieeexplore.ieee.org/abstract/document/9796428)**. 
	- **Erik Johannes Husom**, Simeon Tverdal, Arda Goknil, Sagar Sen.  
	- 2022 IEEE/ACM 1st International Conference on AI Engineering--Software Engineering for AI (CAIN). 2022-05-16.
	- [Link to presentation
  slides](slides/talk-2022-05-cain-unsupervised-learning-pipeline.pdf).
- 4\. **[Deep learning to predict power output from respiratory inductive plethysmography data](https://doi.org/10.1002/ail2.65)**. 
	- **Erik Johannes Husom**, Pierre Bernabé, Sagar Sen. 
	- Applied AI Letters. 2022-04. Journal paper.
- 3\. **[On The Reliability Of Machine Learning Applications In Manufacturing Environments](https://doi.org/10.48550/arxiv.2112.06986)**. 
	- Nicolas Jourdan, Sagar Sen, **Erik Johannes Husom**, Enrique Garcia-Ceja, Tobias Biegel, Joachim Metternich. 
	- Workshop on Distribution Shifts, 35th Conference on Neural Information Processing Systems (NeurIPS 2021). 2021-12-19.
- 2\. **[Deep learning to estimate power output from breathing](https://www.duo.uio.no/bitstream/handle/10852/87270/husom_erik_johannes_master_thesis.pdf?sequence=8)**.  
	- **Erik Johannes Husom**.
	- 2021-06. MSc Thesis.
- 1\. **[DeepVentilation: Learning to Predict Physical Effort from Breathing](https://doi.org/10.24963/ijcai.2020/753)**. 
	- Sagar Sen, Pierre Bernabé, **Erik Johannes B. L. G. Husom**.
	- Proceedings of the Twenty-Ninth International Joint Conference on Artificial Intelligence. 2020-07.
