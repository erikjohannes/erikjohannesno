## Now

What am I up to right now?

- Exploring how to make AI "greener", i.e., more sustainable and less resource-consuming.
- Preparing for [skimo](https://en.wikipedia.org/wiki/Ski_mountaineering) season.

Last updated Nov 2023.

<!-- Inpsired by [NowNowNow](https://nownownow.com/about). -->
