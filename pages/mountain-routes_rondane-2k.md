## [Mountain routes](mountain-routes.html) / Rondane 2K

A classic route linking all summits above 2000 masl of the Rondane mountain range in Norway.
Fastest performance is registered at [fastestknowntime.com](https://fastestknowntime.com/route/rondane-2k-marathon-norway)


### Key info

- Distance: 39-45 km (depending in route choice)
- Elevation gain: 4500-4800 m
- Start/end: Rondvassbu
- Summits:
    - Veslesmeden (2015 masl)
    - Storsmeden (2016 masl)
    - Trolltind (2018 masl)
    - Digerronden (2016 masl)
    - Midtronden Vest (2060 masl)
    - Midtronden Øst (2042 masl)
    - Høgronden (2115 masl)
    - Rondeslottet (2178 masl)
    - Vinjeronden (2044 masl)
    - Storronden (2138 masl)


### Description

The route starts and ends at the lodge Rondvassbu.
Traditionally, the circuit is done clockwise with no out-and-backs, visiting the summits in the order listed above.
The two fastest known times were achieved with variations on the traditional route, and the fastest time was done counter-clockwise (see GPS-tracks below for details).
Most of the route is off-trail and goes through very rocky terrain.
There are also a couple of easy scrambling sections between Veslesmeden and Storsmeden, and between Storsmeden and Trolltind.

<link rel="stylesheet" href="js/leaflet/leaflet.css" />
<script src="js/leaflet/leaflet.js"></script>
<script src="js/gpx.js"></script>

<div id="map" style="height: 400px; width: 100%;"></div>
<noscript>
<style> #map {display:none;} </style>
<img alt="GPS track of the FKT for Rondane 2K." src="posts/20200627-rondane/00-kart.png")/> 
</noscript>
<figcaption><em>Map: GPS track of the FKT for Rondane 2K. The traditional route differs from this by starting with Veslesmeden -> Storsmeden -> Trolltind, then looping on the north side of Nørdre Smedhamran before continuing the circuit with Digerronden and the rest of the summits.</em></figcaption>

<script>
        var map = L.map('map');//.setView([60.14, 10.25], 11);
        L.tileLayer('https://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=topo4&zoom={z}&x={x}&y={y}', {
            attribution: '<a href="http://www.kartverket.no/">Kartverket</a>'
        }).addTo(map);
        var gpx = 'gpx/mountain-route_rondane-2k-husom.gpx'; 
        new L.GPX(gpx, {
            async: true,
            marker_options: {
                startIconUrl: 'img/pin-icon-start.png',
                endIconUrl:   'img/pin-icon-end.png',
                shadowUrl:    'img/pin-shadow.png',
                //clickable: true,
                //showRouteInfo: true
            },
        }).on('loaded', function(e) {
            map.fitBounds(e.target.getBounds());
        }).addTo(map);
</script>



### Notable performances

- 1997-08-21: Knut Stabell - 12:42
    - <details>
    <summary>[Report (fjellforum.no)](https://www.fjellforum.no/topic/412-2000-metertur-i-rondane-2k-samlere/?do=findComment&comment=216553)</summary>
    <blockquote>
Hei !  Tok litt tid dette, men her kommer noen opplysninger om turen, - fritt etter dagboka: 21. august 97. Værmelding med varsel om en brukbar dag. Bil fra Oslo til Spranget, sykkel til Rondvassbu. Start 05:00, 22.08. fra Rondvassbu, Veslesmeden-Storsmeden-Sagtinden-Digerronden-Midtronden-Høgronden-Rondeslottet-Vinjeronden-Storronden. Tilbake på Rondvassbu 17:42, rekker en dusj før middag kl 18:00, retur til Spranget og Oslo samme dag.  Mat: 4x4 skiver loff med honning, 2 bananer, en pakke rosiner, noen plastposer med XL1 for å blande med vann under veis. Tilsvarende på 4 halvliters brusflasker. 2 halvlitersflasker Cola (eneste dop). Spiste alt unntatt 2/3 av rosinene.  Gikk lett med gode joggesko og sekk med mat, overtrekksklær lue og votter mm (ca 3 kg).  Startet på vått fjell etter en regnskur om natta. Tørket noe opp utover dagen, men varmt og lummert. Kraftig regn/hagelbyge oppunder toppen på Rondeslottet, gjentakelse oppunder Vinjeronden. Korte pauser under tordenværene. Lite fristende å stå oppreist på topper i tordenvær !!  I dagboka skriver jeg: Ved optimale forhold, - litt kjøligere og uten fotografering og vonde knær, samt helt tørr stein, bør turen kunne gås på ca 11 timer. Jeg brukte 12:42.  Jeg kjenner ikke til om noen har gått turen raskere.  Har ellers aldri løpt maraton, men drevet med endel annen idrett.  Det stemmer ellers at jeg gikk på alle over 2000 på ett år. (primærfaktor 30) PS ! Ta gjerne kontakt dersom du ønsker ytterligere opplysninger om denne eller andre turer.  knut stabell
</blockquote>
    </details>
- ????-??-??: Svein Vole - 11:24
    - [Source (fjellforum.no)](https://www.fjellforum.no/topic/7017-fjellrute-rondane-2k-maraton/)
- 2009-06-??: Jon Arne Hoås – 11:48
    - <details>
    <summary>[Report (fjellforum.no)](https://www.fjellforum.no/topic/7017-fjellrute-rondane-2k-maraton/?do=findComment&comment=86418)</summary>
    <blockquote>
    Gjorde et forsøk på å bestge de ti toppene i Rondane i begynnelsen av Juli i år. Startet 03.53 fra Rondvassbu og var tilbake til utgangspunktet 11 timer og 48 minutter senere. Tok runden i den mest vanlige rekkefølgen: Veslesmeden, Storsmeden, Sagtinden, Digerronden, Midtrondene, Høgronden, Rondeslottet, Vinjeronden og Storronden. Var helt fra start innstilt på å gjennomføre turen så fort som mulig, så foruten den nødvendige bildedokumentasjonen på toppene og vannfyllingene ble det lite med pauser. Hele turen forløp i godt vær og uten store problemer. Riktignok var jeg godt nede i kjelleren på de siste tre toppene, men det hører vel med på en slik tur.
    </blockquote>
    </details>
- 2010-07-19: Sondre Kvambekk - 11:41:10
    - [Report (peakbook.org)](https://peakbook.org/no/tour/1026/Rondane+2K-maraton.html)
- 2014-07-10: Ola Hovdenak - 09:15
    - [Report (peakbook.org)](https://peakbook.org/no/tour/92982/Rondane+2k-marathon.html)
- 2018-06-02: Gjermund Nordskar - 09:00
    - [Report (peakbook.org)](https://peakbook.org/no/tour/261812/Rondane+2k+maraton.html)
- 2018-06-09: Sylvia Nordskar - 11:30 - **current FKT (female)**
    - [FKT link (fastestknowntime.com)](https://fastestknowntime.com/fkt/sylvia-nordskar-rondane-2k-marathon-norway-2018-06-09)
    - [Report (instagram.com)](https://www.instagram.com/p/Bj18nQhA0St/?img_index=1)
    - [Report (strava.com)](https://www.strava.com/activities/1635595258)
- 2018-07-17: Vegard Fredheim - 08:48:13
    - [FKT link (fastestknowntime.com)](https://fastestknowntime.com/fkt/vegard-fredheim-rondane-2k-marathon-norway-2018-07-17)
    - [Report (instagram.com)](https://www.instagram.com/p/BlYMZtggG9w/?img_index=1)
    - [Report (strava.com)](https://www.strava.com/activities/1709269765)
    - [Video (vimeo.com)](https://vimeo.com/231661227)
    - [GPX](gpx/mountain-routes_rondane-2k-fredheim.gpx)
    - [Article (Runner's World Norway)](https://runnersworld.no/speeddating-i-fjellet-fkt-pa-rondane-2k-maraton/)
- 2020-07-26: Erik Johannes Husom - 08:47:52 - **current FKT (male)**
    - [Report (erikjohannes.no)](https://erikjohannes.no/posts/20200627-rondane/)
    - [FKT link (fastestknowntime.com)](https://fastestknowntime.com/fkt/erik-johannes-husom-rondane-2k-marathon-norway-2020-06-27)
    - [GPX](gpx/mountain-routes_rondane-2k-husom.gpx)
- 2021-06-26: Didrik Hermansen – 09:51
    - <details>
    <summary>[Report (strava.com)](https://www.strava.com/activities/5533550651)</summary>
    <blockquote>Navigerte meg dessverre helt bort i tåka og mista huet litt etter det. Skal man prøve seg bør du unngå tåke (dårlig sikt og glatte steiner) Rekognosere Veslesmeden,Storsmeden og Trolltind partiet på forhånd og vurdere å løpe motsatt vei av det jeg gjorde. Lykke til💪</blockquote>
    </details>
    - [Video](https://www.facebook.com/hokuspokuskreftifokus/videos/skikkelig-kult-%C3%A5-ha-med-didrik-hermansen-p%C3%A5-denne-turen-som-fullf%C3%B8rte-de-10-topp/865581504042188)
    - [GPX](gpx/mountain-routes_rondane-2k-hermansen.gpx)
- 2021-07-16: Aasmund Kjøllmoen Steien – 10:32
    - [Report (strava.com)](https://www.strava.com/activities/5637280003)
    - [GPX](gpx/mountain-routes_rondane-2k-steien.gpx)


<!-- --- -->


<!-- - <details><summary>1997-08-21: Knut Stabell - 12:42</summary> -->
<!--     - <details> -->
<!--     <summary>[Report (fjellforum.no)](https://www.fjellforum.no/topic/412-2000-metertur-i-rondane-2k-samlere/?do=findComment&comment=216553)</summary> -->
<!--     <blockquote> -->
<!-- Hei !  Tok litt tid dette, men her kommer noen opplysninger om turen, - fritt etter dagboka: 21. august 97. Værmelding med varsel om en brukbar dag. Bil fra Oslo til Spranget, sykkel til Rondvassbu. Start 05:00, 22.08. fra Rondvassbu, Veslesmeden-Storsmeden-Sagtinden-Digerronden-Midtronden-Høgronden-Rondeslottet-Vinjeronden-Storronden. Tilbake på Rondvassbu 17:42, rekker en dusj før middag kl 18:00, retur til Spranget og Oslo samme dag.  Mat: 4x4 skiver loff med honning, 2 bananer, en pakke rosiner, noen plastposer med XL1 for å blande med vann under veis. Tilsvarende på 4 halvliters brusflasker. 2 halvlitersflasker Cola (eneste dop). Spiste alt unntatt 2/3 av rosinene.  Gikk lett med gode joggesko og sekk med mat, overtrekksklær lue og votter mm (ca 3 kg).  Startet på vått fjell etter en regnskur om natta. Tørket noe opp utover dagen, men varmt og lummert. Kraftig regn/hagelbyge oppunder toppen på Rondeslottet, gjentakelse oppunder Vinjeronden. Korte pauser under tordenværene. Lite fristende å stå oppreist på topper i tordenvær !!  I dagboka skriver jeg: Ved optimale forhold, - litt kjøligere og uten fotografering og vonde knær, samt helt tørr stein, bør turen kunne gås på ca 11 timer. Jeg brukte 12:42.  Jeg kjenner ikke til om noen har gått turen raskere.  Har ellers aldri løpt maraton, men drevet med endel annen idrett.  Det stemmer ellers at jeg gikk på alle over 2000 på ett år. (primærfaktor 30) PS ! Ta gjerne kontakt dersom du ønsker ytterligere opplysninger om denne eller andre turer.  knut stabell -->
<!-- </blockquote> -->
<!--     </details> -->
<!--     </details> -->
<!-- - <details><summary>????-??-??: Svein Vole - 11:24</summary> -->
<!--     - [Source (fjellforum.no)](https://www.fjellforum.no/topic/7017-fjellrute-rondane-2k-maraton/) -->
<!--     </details> -->
<!-- - <details><summary>2009-06-??: Jon Arne Hoås – 11:48</summary> -->
<!--     - <details> -->
<!--     <summary>[Report (fjellforum.no)](https://www.fjellforum.no/topic/7017-fjellrute-rondane-2k-maraton/?do=findComment&comment=86418)</summary> -->
<!--     <blockquote> -->
<!--     Gjorde et forsøk på å bestge de ti toppene i Rondane i begynnelsen av Juli i år. Startet 03.53 fra Rondvassbu og var tilbake til utgangspunktet 11 timer og 48 minutter senere. Tok runden i den mest vanlige rekkefølgen: Veslesmeden, Storsmeden, Sagtinden, Digerronden, Midtrondene, Høgronden, Rondeslottet, Vinjeronden og Storronden. Var helt fra start innstilt på å gjennomføre turen så fort som mulig, så foruten den nødvendige bildedokumentasjonen på toppene og vannfyllingene ble det lite med pauser. Hele turen forløp i godt vær og uten store problemer. Riktignok var jeg godt nede i kjelleren på de siste tre toppene, men det hører vel med på en slik tur. -->
<!--     </blockquote> -->
<!--     </details> -->
<!--     </details> -->
<!-- - <details><summary>2014-07-10: Ola Hovdenak - 09:15</summary> -->
<!--     - [Report (peakbook.org)](https://peakbook.org/no/tour/92982/Rondane+2k-marathon.html) -->
<!--     </details> -->
<!-- - <details><summary>2018-06-02: Gjermund Nordskar - 09:00</summary> -->
<!--     - [Report (peakbook.org)](https://peakbook.org/no/tour/261812/Rondane+2k+maraton.html) -->
<!--     </details> -->
<!-- - <details><summary>2018-06-09: Sylvia Nordskar - 11:30 - **current FKT (female)**</summary> -->
<!--     - [FKT link (fastestknowntime.com)](https://fastestknowntime.com/fkt/sylvia-nordskar-rondane-2k-marathon-norway-2018-06-09) -->
<!--     </details> -->
<!-- - <details><summary>2018-07-17: Vegard Fredheim - 08:48:13</summary> -->
<!--     - [FKT link (fastestknowntime.com)](https://fastestknowntime.com/fkt/vegard-fredheim-rondane-2k-marathon-norway-2018-07-17) -->
<!--     - [Report (instagram.com)](https://www.instagram.com/p/BlYMZtggG9w/?img_index=1) -->
<!--     - [Report (strava.com)](https://www.strava.com/activities/1709269765) -->
<!--     - [Video (vimeo.com)](https://vimeo.com/231661227) -->
<!--     - [GPX](gpx/mountain-routes_rondane-2k-fredheim.gpx) -->
<!--     - [Article (Runner's World Norway)](https://runnersworld.no/speeddating-i-fjellet-fkt-pa-rondane-2k-maraton/) -->
<!--     </details> -->
<!-- - <details><summary>2020-07-26: Erik Johannes Husom - 08:47:52 - **current FKT (male)**</summary> -->
<!--     - [Report (erikjohannes.no)](https://erikjohannes.no/posts/20200627-rondane/) -->
<!--     - [FKT link (fastestknowntime.com)](https://fastestknowntime.com/fkt/erik-johannes-husom-rondane-2k-marathon-norway-2020-06-27) -->
<!--     </details> -->
<!-- - <details><summary>2021-06-26: Didrik Hermansen – 09:51</summary> -->
<!--     - <details> -->
<!--     <summary>[Report (strava.com)](https://www.strava.com/activities/5533550651)</summary> -->
<!--     <blockquote>Navigerte meg dessverre helt bort i tåka og mista huet litt etter det. Skal man prøve seg bør du unngå tåke (dårlig sikt og glatte steiner) Rekognosere Veslesmeden,Storsmeden og Trolltind partiet på forhånd og vurdere å løpe motsatt vei av det jeg gjorde. Lykke til💪</blockquote> -->
<!--     </details> -->
<!--     - [Video](https://www.facebook.com/hokuspokuskreftifokus/videos/skikkelig-kult-%C3%A5-ha-med-didrik-hermansen-p%C3%A5-denne-turen-som-fullf%C3%B8rte-de-10-topp/865581504042188) -->
<!--     - [GPX](gpx/mountain-routes_rondane-2k-hermansen.gpx) -->
<!--     </details> -->
<!-- - <details><summary>2021-07-16: Aasmund Kjøllmoen Steien – 10:32</summary> -->
<!--     - [Report (strava.com)](https://www.strava.com/activities/5637280003) -->
<!--     - [GPX](gpx/mountain-routes_rondane-2k-steien.gpx) -->
<!--     </details> -->
