# Knowledge


## Artificial Intelligence (AI) and Machine Learning (ML)


### Generative AI


#### Large Language Models


##### Models

###### Phi3 (Microsoft)

- [Ollama - Phi3](https://ollama.com/library/phi3)
    - 3B, 14B

###### qwen2 (Alibaba)

- [Ollama - qwen2](https://ollama.com/library/qwen2)
    - 0.5B, 1.5B, 7B


##### Datasets and Benchmarks

Lists/overviews of datasets and benchmarks:

- [llm benchmarks (GitHub)](https://github.com/leobeeson/llm_benchmarks)
- [Awesome Human Preference Datasets for LLM (GitHub)](https://github.com/glgh/awesome-llm-human-preference-datasets?tab=readme-ov-file)


Summarization:

- [CNN / Dailymail news articles](Text summarization of news articles: https://huggingface.co/datasets/abisee/cnn_dailymail)
- [Summarization dataset used by OpenAI for training model](https://huggingface.co/datasets/openai/summarize_from_feedback)

Question answering / explaining concepts / trivia questions:

- Explaining concepts / question answering: https://huggingface.co/datasets/Hello-SimpleAI/HC3
- [TriviQA](https://huggingface.co/datasets/mandarjoshi/trivia_qa)

Multiple choice:

- [sciq (HuggingFace](https://huggingface.co/datasets/allenai/sciq)
    - Multiple choice questions on science
- [Measuring Massive Multitask Language Understanding (MMLU)](https://github.com/hendrycks/test)
    - Multiple choice questions across 57 different subjects, ranging from STEM to social sciences

## Computer hardware


### Energy and power measurements, monitoring, usage


#### Intel: Running Average Power Limit (RAPL)

- RAPL in Action :Experiences in Using RAPL for Power Measurements (Khan 2018). [Paper](https://helda.helsinki.fi/server/api/core/bitstreams/bdc6c9a5-74d4-494b-ae83-860625a665ce/content)

